import json
import numpy as np
import random
import torch
import warnings

from pathlib import Path

from beadnet_source import utils
from beadnet_source.bead_dataset import BeadDataset
from beadnet_source.beadnet_gui import run_gui
from beadnet_source.mytransforms import augmentors
from beadnet_source.train_inference import train, eval_model, inference
from beadnet_source.unets import build_unet


warnings.filterwarnings("ignore", category=UserWarning)


def main():

    random.seed()
    np.random.seed()

    # Load settings
    with open(Path.cwd() / 'settings.json') as f:
        settings = json.load(f)

    # Set device for using CPU or GPU
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    if str(device) == 'cuda':
        torch.backends.cudnn.benchmark = True
    num_gpus = torch.cuda.device_count()

    # Train/evaluate model or make predictions
    mode = settings['mode']

    if mode == 'train':  # Train model from scratch

        # Make directory for the trained models
        Path(settings['path_models']).mkdir(exist_ok=True)

        for apply_dilation in [True, False]:  # Always train a model with and without dilated label seeds

            for i in range(settings['iterations']):  # Train multiple models

                run_name = utils.unique_path(Path(settings['path_models']), 'beadnet_model_{:03d}.pth').stem

                train_configs = {'apply_dilation': apply_dilation,
                                 'batch_size': settings['batch_size'],
                                 'break_condition': settings['break_condition'],
                                 'filters': settings['filters'],
                                 'learning_rate': settings['learning_rate'],
                                 'max_epochs': settings['max_epochs'],
                                 'num_gpus': num_gpus,
                                 'run_name': run_name,
                                 'path_models': Path(settings['path_models'])}

                net = build_unet(device=device, num_gpus=num_gpus, filters=train_configs['filters'])

                # The training images are assumed to be uint16 crops of a min-max normalized image
                data_transforms = augmentors(augmentation='train', min_value=0, max_value=65535)

                train_configs['data_transforms'] = str(data_transforms)

                # Load training and validation set
                datasets = {x: BeadDataset(root_dir=Path(settings['path_bead_dataset']),
                                           mode=x,
                                           transform=data_transforms[x],
                                           apply_dilation=train_configs['apply_dilation'])
                            for x in ['train', 'val']}

                # Train model
                train(net=net, datasets=datasets, configs=train_configs, device=device)

                # Write information to txt-file
                utils.write_train_info(configs=train_configs)

    elif mode == 'eval':  # Eval all trained models on the test data set

        # Get paths of all trained models
        models = sorted(Path(settings['path_models']).glob('*.pth'))

        # Dictionaries for the results
        model_metrics, model_metrics_bc = {}, {}

        # Evaluate all trained models
        for model in models:

            # Make directories for results on the test data set
            Path.mkdir(model.parent / model.stem, exist_ok=True)

            # Load training configs
            with open(model.parent / (model.stem + '.json')) as f:
                train_configs = json.load(f)

            dataset = BeadDataset(root_dir=Path(settings['path_bead_dataset']),
                                  mode='test',
                                  transform=augmentors(augmentation='eval', min_value=0, max_value=65535) )

            # Predict test images and calculate metrics
            results = eval_model(model=model,
                                 num_gpus=num_gpus,
                                 filters=train_configs['filters'],
                                 dataset=dataset,
                                 device=device)

            model_metrics[model.stem] = results['metrics']
            model_metrics_bc[model.stem] = results['metrics_border_corrected']

            # Append metrics to the training configs file
            utils.write_eval_info(model_path=model.parent, file_name=model.stem, results=results, mode='single_model')

        # Write file with metrics of all trained models
        utils.write_eval_info(model_path=model.parent, file_name='metrics', results=model_metrics, mode='all')
        utils.write_eval_info(model_path=model.parent, file_name='metrics_bc', results=model_metrics_bc, mode='all')

    elif mode == 'inference':  # Predict images using best model (on the test data set)

        # Make result path
        Path.mkdir(Path(settings['path_data']) / 'results', exist_ok=True)

        # Find best model (best micro F-score)
        with open(Path(settings['path_models']) / 'metrics_bc.json') as f:
            metrics_bc = json.load(f)
        print('Looking for best model.')
        best_score = 0
        for model in metrics_bc:
            if metrics_bc[model]['Q_F_micro'] > best_score:
                best_score = metrics_bc[model]['Q_F_micro']
                best_model = model

        # Use best model for inference
        print('Inference using {}.'.format(best_model))
        model = Path(settings['path_models']) / (best_model + '.pth')
        with open(model.parent / (model.stem + '.json')) as f:
            filters = json.load(f)['filters']
        num_beads = inference(data_path=Path(settings['path_data']),
                              upsampling_factor=settings['upsampling_factor'],
                              model=model,
                              filters=filters,
                              device=device,
                              num_gpus=num_gpus,
                              color_channel=settings['color_channel'],
                              normalization=settings['normalization'])

        # Write result file with number of beads for each image
        utils.write_inference_results(results=num_beads, path=Path(settings['path_data']) / 'results')

    elif mode == 'inference_ensemble':  # Predict images using the three best models (on the test data set)

        # Make result path
        Path.mkdir(Path(settings['path_data']) / 'results', exist_ok=True)

        # Find best models (best micro F-score)
        with open(Path(settings['path_models']) / 'metrics_bc.json') as f:
            metrics_bc = json.load(f)
        print('Looking for best three models.')
        scores, models = [], []
        for model in metrics_bc:
            scores.append(metrics_bc[model]['Q_F_micro'])
            models.append(model)
        idcs = np.argsort(np.array(scores))
        # scores = [scores[i] for i in idcs]
        models = [models[i] for i in idcs]

        # Use three best models for interference
        num_beads = {}
        for best_model in models[-3:]:

            print('Inference using {}.'.format(best_model))
            model = Path(settings['path_models']) / (best_model + '.pth')
            with open(model.parent / (model.stem + '.json')) as f:
                filters = json.load(f)['filters']

            num_beads[best_model] = inference(data_path=Path(settings['path_data']),
                                              upsampling_factor=settings['upsampling_factor'],
                                              model=model,
                                              filters=filters,
                                              device=device,
                                              num_gpus=num_gpus,
                                              color_channel=settings['color_channel'],
                                              normalization=settings['normalization'])

        # Rearrange the results
        results = {}
        for file in num_beads[best_model]:
            file_num_beads = []
            for model in num_beads:
                file_num_beads.append(num_beads[model][file])
            results[file] = file_num_beads

        # Write result file with number of beads for each image
        utils.write_inference_results(results=results, path=Path(settings['path_data']) / 'results')

    elif mode == 'gui':

        run_gui(model_path=Path.cwd()/'models',
                training_data_path=Path.cwd()/'beadnet_dataset',
                device=device,
                num_gpus=num_gpus)

    else:

        raise Exception('Unknown mode. Try: train, eval, gui, inference, inference_ensemble')


if __name__ == "__main__":

    main()

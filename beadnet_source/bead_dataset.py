import numpy as np
import tifffile as tiff
from pathlib import Path
from torch.utils.data import Dataset


class BeadDataset(Dataset):
    """ Bead data set for bead detection """

    def __init__(self, root_dir, mode='train', apply_dilation=False, transform=lambda x: x):
        """

        :param root_dir: Path to the dataset containing a train, val and test directory.
            :type root_dir: pathlib.PosixPath or pathlib.WindowsPath
        :param mode: Use training, validation or test data set
            :type mode: str
        :param apply_dilation: Use dilated seeds or not.
            :type apply_dilation: bool
        :param transform: Transforms/augmentations to apply
            :type torchvision.transforms.Compose
        """

        self.img_ids = sorted(Path.joinpath(root_dir, mode).glob('*upsampled.tif'))
        self.mode = mode
        self.root_dir = root_dir
        self.transform = transform
        self.apply_dilation = apply_dilation

    def __len__(self):
        return len(self.img_ids)

    def __getitem__(self, idx):

        img_id = self.img_ids[idx]

        img = tiff.imread(str(img_id))

        if self.mode == 'test':
            label_id = img_id.parent / (img_id.stem.split('img_upsampled')[0] + 'seeds_metric.tif')
            label = tiff.imread(str(label_id))
        else:
            if self.apply_dilation:
                label_id = img_id.parent / (img_id.stem.split('img_upsampled')[0] + 'seeds_dilated.tif')
                label = (tiff.imread(str(label_id)) > 0).astype(np.uint8)
            else:
                label_id = img_id.parent / (img_id.stem.split('img_upsampled')[0] + 'seeds.tif')
                label = (tiff.imread(str(label_id)) > 0).astype(np.uint8)

        sample = {'image': img, 'label': label, 'id': img_id.stem}

        sample = self.transform(sample)

        return sample

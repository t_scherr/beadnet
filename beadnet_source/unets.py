import torch
import torch.nn as nn


def build_unet(device, num_gpus, ch_in=1, ch_out=1, filters=[64, 128, 256, 512], gui=None):
    """ Set up a U-net model.

    :param device: Use GPU or CPU.
        :type device: torch device
    :param num_gpus: Number of GPUs to use.
        :type num_gpus: int
    :param ch_in: Number of color channels of the input, e.g., 3 for rgb images.
        :type ch_in: int
    :param ch_out: Number of output channels, e.g., 1 for seed detection.
        :type ch_out: int
    :param filters: Number of feature maps in the encoder of the U-net (and correspondingly for the decoder).
        :type filters: list
    :return: model, exit status
    """

    # Get U-Net architecture
    model = UNet(ch_in=ch_in, ch_out=ch_out, filters=filters)

    # Use multiple GPUs if available
    if num_gpus > 1:
        model = nn.DataParallel(model)

    # Move model to used device (GPU or CPU)
    try:
        model = model.to(device)
    except:
        if gui is not None and num_gpus > 0:
            gui.output_text_edit.append('Could not move model to GPU. Try to use CPU.')
            gui.output_text_edit.repaint()
            return 1, 1
        return 1, 1

    return model, 0


class ConvBlock(nn.Module):
    """ Basic convolutional block of a unet. """

    def __init__(self, ch_in, ch_out):
        """

        :param ch_in: Number of channels of the block's input.
            :type ch_in: int
        :param ch_out: Number of channels of the block's output.
            :type ch_out: int
        """

        super().__init__()
        self.conv = list()
        self.conv.append(nn.Conv2d(ch_in, ch_out, kernel_size=3, stride=1, padding=1, bias=True))
        self.conv.append(nn.ReLU(inplace=True))
        self.conv.append(nn.BatchNorm2d(ch_out))
        self.conv.append(nn.Conv2d(ch_out, ch_out, kernel_size=3, stride=1, padding=1, bias=True))
        self.conv.append(nn.ReLU(inplace=True))
        self.conv.append(nn.BatchNorm2d(ch_out))
        self.conv = nn.Sequential(*self.conv)

    def forward(self, x):
        """

        :param x: Block input (image or feature maps).
            :type x:
        :return: Block output (feature maps).
        """
        for i in range(len(self.conv)):
            x = self.conv[i](x)

        return x


class TranspConvBlock(nn.Module):
    """ Upsampling block of a unet (with transposed convolutions). """

    def __init__(self, ch_in, ch_out):
        """

        :param ch_in: Number of channels of the block's input.
            :type ch_in: int
        :param ch_out: Number of channels of the block's output.
            :type ch_out: int
        """
        super().__init__()

        self.up = nn.Sequential(nn.ConvTranspose2d(ch_in, ch_out, kernel_size=2, stride=2))
        self.bn = nn.BatchNorm2d(ch_out)

    def forward(self, x):
        """

        :param x: Block input (image or feature maps).
            :type x:
        :return: Block output (upsampled feature maps).
        """
        x = self.up(x)
        x = self.bn(x)

        return x
    

class UNet(nn.Module):
    """Implementation of the U-Net architecture.

    Changes to original architecture: zero padding is used to keep the image and feature map dimensions constant. Batch
        normalization is applied. Transposed convolutions are used for the upsampling.

    Reference: Olaf Ronneberger et al. "U-Net: Convolutional Neural Networks for Biomedical Image Segmentation". In:
        International Conference on Medical Image Computing and Computer-Assisted Intervention. Springer. 2015.

    """

    def __init__(self, ch_in=1, ch_out=1, filters=[64, 128, 256, 512]):
        """

        :param ch_out: Channels/Number of feature maps of the output.
            :type ch_out: int
        :param ch_in: Channels/Number of feature maps of the input.
            :type ch_in: int
        :param filters: Number of feature maps used in every encoder block.
            :type filters: list
        """

        super().__init__()
        
        self.ch_in = ch_in
        self.ch_out = ch_out
        self.filters = filters
            
        # Maximum pooling
        self.pooling = nn.MaxPool2d(kernel_size=2, stride=2)
        
        # Encoder
        self.encoderConv = nn.ModuleList()
        
        # First encoder block
        self.encoderConv.append(ConvBlock(ch_in=self.ch_in, ch_out=self.filters[0]))
        # Remaining encoder blocks
        for i_depth in range(1, len(self.filters)):
            self.encoderConv.append(ConvBlock(ch_in=self.filters[i_depth-1], ch_out=self.filters[i_depth]))
            
        # Transposed convolutions
        self.upconv = nn.ModuleList()
        for i_depth in range(len(self.filters)-1):
            self.upconv.append(TranspConvBlock(self.filters[-i_depth-1], self.filters[-i_depth - 2]))
        
        # Decoder
        self.decoderConv = nn.ModuleList()
        for i_depth in range(len(self.filters) - 1):
            self.decoderConv.append(ConvBlock(ch_in=self.filters[-i_depth - 1], ch_out=self.filters[-i_depth - 2]))
        
        # Last 1x1 convolution
        self.decoderConv.append(nn.Conv2d(self.filters[0], self.ch_out, kernel_size=1, stride=1, padding=0))

    def forward(self, x):
        """

        :param x: Model input.
            :type x:
        :return: Model output / prediction.
        """

        x_temp = list()
        # Decoder
        for i in range(len(self.encoderConv)-1):
            x = self.encoderConv[i](x)
            x_temp.append(x)
            x = self.pooling(x)
        x = self.encoderConv[-1](x)

        # Encoder
        x_temp = list(reversed(x_temp))
        for i in range(len(self.filters)-1):
            x = self.upconv[i](x)
            x = torch.cat([x, x_temp[i]], 1)
            x = self.decoderConv[i](x)
        x = self.decoderConv[-1](x)
        
        return x

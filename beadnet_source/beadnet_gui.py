import json
import numpy as np
import os
import subprocess
import sys
import tifffile as tiff
import torch
import webbrowser

from imageio import imread
from pathlib import Path
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QPalette, QColor, QIntValidator, QKeySequence, QPixmap, QImage
from PyQt5.QtCore import Qt, QFileInfo
from random import randint
from scipy.ndimage import binary_dilation
from scipy.ndimage.morphology import generate_binary_structure
from skimage.io import imread
from skimage.measure import label
from skimage.morphology import watershed
from skimage.transform import rescale

from beadnet_source import utils
from beadnet_source.bead_dataset import BeadDataset
from beadnet_source.mytransforms import augmentors
from beadnet_source.train_inference import eval_model, inference, train
from beadnet_source.unets import build_unet


class BeadNetMainWindow(QWidget):
    def __init__(self, device, num_gpus, model_path='', train_path='', parent=None):
        super().__init__()

        self.device = device
        self.num_gpus = num_gpus

        self.setWindowTitle("BeadNet")

        # define QWidgets
        self.data_path_button = QPushButton('Open data dir')
        self.model_path_button = QPushButton('Open model dir')
        self.train_path_button = QPushButton('Open training dir')
        self.data_path_line_edit = QLineEdit()
        self.model_path_line_edit = QLineEdit(str(model_path))
        self.train_path_line_edit = QLineEdit(str(train_path))

        self.color_channel_label = QLabel('Color channel:')
        color_group = QButtonGroup(self)
        self.red_channel_checkbox = QRadioButton("r")
        color_group.addButton(self.red_channel_checkbox)
        self.green_channel_checkbox = QRadioButton("g")
        color_group.addButton(self.green_channel_checkbox)
        self.blue_channel_checkbox = QRadioButton("b")
        color_group.addButton(self.blue_channel_checkbox)
        self.grayscale_checkbox = QRadioButton("grayscale")
        color_group.addButton(self.grayscale_checkbox)
        self.grayscale_checkbox.setChecked(True)

        self.diameter_label = QLabel('Diameter range [px]:')
        diameter_group = QButtonGroup(self)
        self.fourfold_upsampling_checkbox = QRadioButton("1 - 4")
        diameter_group.addButton(self.fourfold_upsampling_checkbox)
        self.twofold_upsampling_checkbox = QRadioButton("5 - 8")
        diameter_group.addButton(self.twofold_upsampling_checkbox)
        self.no_upsampling_checkbox = QRadioButton("> 8")
        diameter_group.addButton(self.no_upsampling_checkbox)

        self.inference_checkbox = QCheckBox('Ensemble mode')

        self.device_label = QLabel('Device:')
        device_group = QButtonGroup(self)
        self.device_cpu_checkbox = QRadioButton("cpu", )
        device_group.addButton(self.device_cpu_checkbox)
        self.device_gpu_checkbox = QRadioButton("gpu")
        device_group.addButton(self.device_gpu_checkbox)
        if self.device.type == 'cpu':
            self.device_cpu_checkbox.setChecked(True)
            self.device_gpu_checkbox.setCheckable(False)
        else:
            self.device_gpu_checkbox.setChecked(True)

        self.create_train_data_button = QPushButton('Create new training data')
        self.train_button = QPushButton('Train new models')
        self.eval_button = QPushButton('Evaluate models')
        self.inference_button = QPushButton('Inference')

        self.output_text_edit = QTextEdit('Press F1 for help.')
        self.output_text_edit.setReadOnly(True)

        # Status bar
        self.status_bar = QStatusBar()
        self.status_bar.showMessage('Ready')

        # Train settings window
        self.train_settings = QDialog()
        self.train_settings.setWindowTitle('BeadNet Training')
        self.train_settings_batchsize_label = QLabel('Batch size (1-16):')
        self.train_settings_batchsize_line_edit = QLineEdit('4')
        self.train_settings_batchsize_line_edit.setAlignment(Qt.AlignRight)
        self.train_settings_batchsize_line_edit.setValidator(QIntValidator(1, 16))
        self.train_settings_epochs_label = QLabel('Epochs (20-200):')
        self.train_settings_epochs_line_edit = QLineEdit('100')
        self.train_settings_epochs_line_edit.setAlignment(Qt.AlignRight)
        self.train_settings_epochs_line_edit.setValidator(QIntValidator(20, 200))
        self.train_settings_iterations_label = QLabel('Iterations (1-25):')
        self.train_settings_iterations_line_edit = QLineEdit('5')
        self.train_settings_iterations_line_edit.setAlignment(Qt.AlignRight)
        self.train_settings_iterations_line_edit.setValidator(QIntValidator(1, 25))
        self.train_settings_train_button = QPushButton('Train')
        self.train_settings_back_button = QPushButton('Back')

        # Create training data window
        self.create_train_data = QDialog()
        self.create_train_data.setWindowTitle('BeadNet Training Data Creation')
        self.create_train_data_crop_button = QPushButton('Create new crops')
        self.create_train_data_annotate_button = QPushButton('Annotate')
        self.create_train_data_convert_button = QPushButton('Convert to data set')
        self.stop_train = False

        # Select train data crops window
        self.select_crops = QDialog()
        self.select_crops.setWindowTitle('Select Training Crops')
        self.select_crops_img = QLabel('')
        self.select_crops_img.setAlignment(Qt.AlignCenter)
        self.select_crops_filename_label = QLabel('')
        self.select_crops_filename_label.setAlignment(Qt.AlignCenter)
        self.select_crops_num_crops_label = QLabel('Number of crops:')
        self.select_crops_counter_label = QLabel('0')
        self.select_crops_accept_button = QPushButton('&Accept')
        self.select_crops_reject_button = QPushButton('&Reject')
        self.select_crops_ids_denied = []

        # init layout and color
        self.init_ui()
        self.init_dark_mode()

        # Shortcuts
        QShortcut(QKeySequence(Qt.Key_F1), self, self.help_shortcut_clicked)
        QShortcut(QKeySequence(Qt.Key_A), self.select_crops, self.select_crops_accept_button_clicked)
        QShortcut(QKeySequence(Qt.Key_R), self.select_crops, self.select_crops_reject_button_clicked)
        QShortcut(QKeySequence("Ctrl+C"), self, self.stop_calculation)

        # connect
        self.data_path_button.clicked.connect(self.data_path_button_clicked)
        self.model_path_button.clicked.connect(self.model_path_button_clicked)
        self.train_path_button.clicked.connect(self.train_path_button_clicked)

        self.create_train_data_button.clicked.connect(self.create_train_data_button_clicked)
        self.train_button.clicked.connect(self.train_button_clicked)
        self.eval_button.clicked.connect(self.eval_button_clicked)
        self.inference_button.clicked.connect(self.inference_button_clicked)

        self.train_settings_back_button.clicked.connect(self.train_settings_back_button_clicked)
        self.train_settings_train_button.clicked.connect(self.train_settings_train_button_clicked)

        self.create_train_data_crop_button.clicked.connect(self.create_train_data_crop_button_clicked)
        self.create_train_data_annotate_button.clicked.connect(self.create_train_data_annotate_button_clicked)
        self.create_train_data_convert_button.clicked.connect(self.create_train_data_convert_button_clicked)

        self.select_crops_accept_button.clicked.connect(self.select_crops_accept_button_clicked)
        self.select_crops_reject_button.clicked.connect(self.select_crops_reject_button_clicked)

        self.show()

    def init_ui(self):
        self.setGeometry(300, 300, 600, 400)

        # Paths (left side)
        create_paths_box = QGroupBox("Paths")
        path_box_layout = QFormLayout()
        path_box_layout.addRow(self.data_path_button, self.data_path_line_edit)
        path_box_layout.addRow(self.model_path_button, self.model_path_line_edit)
        path_box_layout.addRow(self.train_path_button, self.train_path_line_edit)
        create_paths_box.setLayout(path_box_layout)

        # Settings (right side)
        create_settings_box = QGroupBox("Settings")
        color_channel_layout = QHBoxLayout()
        color_channel_layout.addWidget(self.red_channel_checkbox)
        color_channel_layout.addWidget(self.green_channel_checkbox)
        color_channel_layout.addWidget(self.blue_channel_checkbox)
        color_channel_layout.addWidget(self.grayscale_checkbox)

        diameter_layout = QHBoxLayout()
        diameter_layout.addWidget(self.fourfold_upsampling_checkbox)
        diameter_layout.addWidget(self.twofold_upsampling_checkbox)
        diameter_layout.addWidget(self.no_upsampling_checkbox)

        device_layout = QHBoxLayout()
        device_layout.addWidget(self.device_label)
        device_layout.addWidget(self.device_cpu_checkbox)
        device_layout.addWidget(self.device_gpu_checkbox)
        settings_box_layout = QVBoxLayout()
        settings_box_layout.addWidget(self.color_channel_label)
        settings_box_layout.addLayout(color_channel_layout)
        settings_box_layout.addWidget(self.diameter_label)
        settings_box_layout.addLayout(diameter_layout)
        settings_box_layout.addLayout(device_layout)
        settings_box_layout.addWidget(self.inference_checkbox)
        settings_box_layout.addStretch(1)
        create_settings_box.setLayout(settings_box_layout)

        # Bottom
        bottom_buttons_layout = QHBoxLayout()
        bottom_buttons_layout.addWidget(self.create_train_data_button)
        bottom_buttons_layout.addWidget(self.train_button)
        bottom_buttons_layout.addWidget(self.eval_button)
        bottom_buttons_layout.addWidget(self.inference_button)

        # Main Layout
        mainLayout = QGridLayout()
        mainLayout.addWidget(create_paths_box, 0, 0)
        mainLayout.addWidget(create_settings_box, 0, 1)
        mainLayout.addLayout(bottom_buttons_layout, 1, 0, 1, 2)
        mainLayout.addWidget(self.output_text_edit, 2, 0, 1, 2)
        mainLayout.addWidget(self.status_bar, 3, 0, 1, 2)
        mainLayout.setRowStretch(1, 1)
        mainLayout.setRowStretch(2, 1)
        mainLayout.setColumnStretch(0, 3)
        mainLayout.setColumnStretch(1, 1)
        self.setLayout(mainLayout)

        # Train settings layout
        train_layout = QFormLayout()
        train_layout.addRow(self.train_settings_batchsize_label, self.train_settings_batchsize_line_edit)
        train_layout.addRow(self.train_settings_iterations_label, self.train_settings_iterations_line_edit)
        train_layout.addRow(self.train_settings_epochs_label, self.train_settings_epochs_line_edit)
        train_layout.addRow(self.train_settings_back_button, self.train_settings_train_button)
        self.train_settings.setLayout(train_layout)

        # Create new training data menu layout
        create_data_layout = QVBoxLayout()
        create_data_layout.addWidget(self.create_train_data_crop_button)
        create_data_layout.addWidget(self.create_train_data_annotate_button)
        create_data_layout.addWidget(self.create_train_data_convert_button)
        self.create_train_data.setLayout(create_data_layout)

        # Create new training crops layout
        select_crops_layout = QVBoxLayout()
        select_crops_counter_layout = QHBoxLayout()
        select_crops_counter_layout.addWidget(self.select_crops_num_crops_label)
        select_crops_counter_layout.addWidget(self.select_crops_counter_label)
        select_crops_layout.addWidget(self.select_crops_filename_label)
        select_crops_layout.addWidget(self.select_crops_img)
        select_crops_layout.addLayout(select_crops_counter_layout)
        select_crops_layout.addWidget(self.select_crops_accept_button)
        select_crops_layout.addWidget(self.select_crops_reject_button)
        self.select_crops.setLayout(select_crops_layout)

    def init_dark_mode(self):
        self.setPalette(get_dark_pallette())
        self.train_settings.setPalette(get_dark_pallette())
        self.create_train_data.setPalette(get_dark_pallette())
        self.select_crops.setPalette(get_dark_pallette())
        #self.setStyleSheet("QToolTip { color: #ffffff; background-color: #2a82da; border: 1px solid white; }")

    def data_path_button_clicked(self):
        dir_name = QFileDialog.getExistingDirectory(self, "Select Directory")
        self.data_path_line_edit.setText(dir_name)

    def model_path_button_clicked(self):
        dir_name = QFileDialog.getExistingDirectory(self, "Select Directory")
        self.model_path_line_edit.setText(dir_name)

    def train_path_button_clicked(self):
        dir_name = QFileDialog.getExistingDirectory(self, "Select Directory")
        self.train_path_line_edit.setText(dir_name)

    def create_train_data_button_clicked(self):

        self.create_train_data.exec_()

    def create_train_data_crop_button_clicked(self):

        # Check if bead diameter is set
        if not (self.fourfold_upsampling_checkbox.isChecked() or self.twofold_upsampling_checkbox.isChecked() or self.no_upsampling_checkbox.isChecked()):
            self.output_text_edit.append('No diameter range specified.')
            self.create_train_data.close()
            return

        # Check if training dir exists
        train_path = self.train_path_line_edit.text()
        if not QFileInfo(train_path).exists():
            self.output_text_edit.append('Training dir does not exist (to save the crops).')
            self.create_train_data.close()
            return

        # Check if data path exists
        data_path = self.data_path_line_edit.text()
        if not QFileInfo(data_path).exists():
            self.output_text_edit.append('Data dir does not exist.')
            self.create_train_data.close()
            return

        # Check if data path consists images (.png, .tif, .tiff, .jpg)
        # Get images to predict (.tif, .tiff, .png, .jpg)
        extensions = ['.tif*', '.png', '.jpg']
        data_path_files = []
        for extension in extensions:
            data_path_files.extend(list(Path(data_path).glob('*' + extension)))
        self.data_path_files = sorted(data_path_files)
        if len(self.data_path_files) == 0:
            self.output_text_edit.append('No tiff/png/jpg files to create training crops found in data dir.')
            self.create_train_data.close()
            return

        # Check if training dir already contains images to annotate
        if len(list(Path(train_path).glob('*img_upsampled.tif'))) > 0:
            box = QMessageBox()
            box.setWindowTitle('Info')
            box.setText('Training directory already contains images. New crops will be added.')
            box.setPalette(get_dark_pallette())
            box.exec_()
            self.select_crops_counter_label.setNum(len(list(Path(train_path).glob('*img_upsampled.tif'))))

        # self.create_train_data.close()

        # Get first crop and execute select_crops
        Path.mkdir(Path(train_path) / 'non_upsampled_crops', exist_ok=True)
        if self.get_crop() is not None:
            self.select_crops.exec_()

    def get_crop(self):

        # Find color channel
        if self.red_channel_checkbox.isChecked():
            color_channel = 'r'
        elif self.green_channel_checkbox.isChecked():
            color_channel = 'g'
        elif self.blue_channel_checkbox.isChecked():
            color_channel = 'b'
        else:
            color_channel = 'grayscale'

        # Try to get unique crop
        train_path = Path(self.train_path_line_edit.text())
        shots, max_shots = 0, 150
        while shots < max_shots:

            i = randint(0, len(self.data_path_files)-1)

            # Load image
            if self.data_path_files[i].suffix == '.tif' or self.data_path_files[i].suffix == '.tiff':
                img = tiff.imread(str(self.data_path_files[i]))
            else:  # jpg/png
                img = imread(str(self.data_path_files[i]))

            # Get position of the color channel
            if len(img.shape) == 2:  # grayscale image, add pseudo color channel
                img = np.expand_dims(img, axis=-1)
            elif len(img.shape) == 3:  # rgb image
                if img.shape[-1] == 3:  # rgb image with channels last
                    if color_channel == 'r':
                        img = np.expand_dims(img[:, :, 0], axis=-1)
                    elif color_channel == 'g':
                        img = np.expand_dims(img[:, :, 1], axis=-1)
                    elif color_channel == 'b':
                        img = np.expand_dims(img[:, :, 2], axis=-1)
                    elif color_channel == 'grayscale':
                        img = np.expand_dims(np.mean(img, axis=-1), axis=-1)
                elif img.shape[0] == 3:  # rgb image with channels first
                    if color_channel == 'r':
                        img = np.expand_dims(img[0, :, :], axis=-1)
                    elif color_channel == 'g':
                        img = np.expand_dims(img[1, :, :], axis=-1)
                    elif color_channel == 'b':
                        img = np.expand_dims(img[2, :, :], axis=-1)
                    elif color_channel == 'grayscale':
                        img = np.expand_dims(np.mean(img, axis=0), axis=-1)
                elif img.shape[0] == 1:
                    img = np.swapaxes(img, 0, -1)
                elif img.shape[-1] == 1:
                    pass
            else:
                self.output_text_edit.append('Unsupported channels in image {}'.format(self.data_path_files[i].stem))
                self.output_text_edit.repaint()
                self.select_crops.close()
                return

            # Normalization
            img = img.astype(np.float32)
            img_normalized = 65535 * (img - img.min()) / (img.max() - img.min())
            img_normalized = img_normalized.astype(np.uint16)

            # Cropping and upsampling
            if self.fourfold_upsampling_checkbox.isChecked():  # 32x32 crops & fourfold bilinear upsampling
                if img.shape[0] < 64 or img.shape[1] < 64:
                    self.output_text_edit.append('Each image dimension should be > 64 for appropriate cropping.')
                    self.output_text_edit.repaint()
                    self.select_crops.close()
                    return
                a, b = randint(0, np.floor_divide(img.shape[0], 32) - 1), randint(0, np.floor_divide(img.shape[1], 32) - 1)
                img_crop = img_normalized[a*32:(a+1)*32, b*32:(b+1)*32, :]
                img_crop_upsampled = rescale(img_crop, 4, order=2, multichannel=True)
                img_crop_upsampled = np.clip(65535 * img_crop_upsampled, 0, 65535).astype(np.uint16)
            elif self.twofold_upsampling_checkbox.isChecked():  # 64x64 crops with twofold bilinear upsampling
                if img.shape[0] < 128 or img.shape[1] < 128:
                    self.output_text_edit.append('Each image dimension should be > 128 for appropriate cropping.')
                    self.output_text_edit.repaint()
                    self.select_crops.close()
                    return
                a, b = randint(0, np.floor_divide(img.shape[0], 64) - 1), randint(0, np.floor_divide(img.shape[1], 64) - 1)
                img_crop = img_normalized[a * 64:(a + 1) * 64, b * 64:(b + 1) * 64, :]
                img_crop_upsampled = rescale(img_crop, 2, order=2, multichannel=True)
                img_crop_upsampled = np.clip(65535 * img_crop_upsampled, 0, 65535).astype(np.uint16)
            else:  # 128x128 crops without upsampling
                if img.shape[0] < 256 or img.shape[1] < 256:
                    self.output_text_edit.append('Each image dimension should be > 256 for appropriate cropping.')
                    self.output_text_edit.repaint()
                    self.select_crops.close()
                    return
                a, b = randint(0, np.floor_divide(img.shape[0], 128) - 1), randint(0, np.floor_divide(img.shape[1], 128) - 1)
                img_crop = img_normalized[a * 128:(a + 1) * 128, b * 128:(b + 1) * 128, :]
                img_crop_upsampled = img_crop

            if QFileInfo(str(train_path / ('{0}_{1}_{2}_img_upsampled.tif'.format(self.data_path_files[i].stem,
                                                                                  a, b)))).exists():
                shots += 1
                continue
            elif '{0}_{1}_{2}_img_upsampled'.format(self.data_path_files[i].stem, a, b) in self.select_crops_ids_denied:
                continue
            else:

                # Show cropped image
                qimage = QImage((255*img_crop_upsampled.astype(np.float32)/65535).astype(np.uint8),
                                img_crop_upsampled.shape[1], img_crop_upsampled.shape[0], QImage.Format_Indexed8)
                pixmap = QPixmap.fromImage(qimage)
                self.select_crops_img.setPixmap(pixmap)
                self.select_crops_filename_label.setText(self.data_path_files[i].stem + '_{0}_{1}'.format(a, b))
                self.crop = [img_crop, img_crop_upsampled, self.data_path_files[i].stem + '_{0}_{1}'.format(a, b)]
                break

        if shots == max_shots:
            self.output_text_edit.append('Cannot find new unique crops.')
            self.output_text_edit.repaint()
            self.select_crops.close()
            return

        return 0

    def select_crops_accept_button_clicked(self):

        train_path = Path(self.train_path_line_edit.text())

        # Increase counter
        self.select_crops_counter_label.setNum(int(self.select_crops_counter_label.text()) + 1)
        self.select_crops_counter_label.repaint()

        # Save crop
        tiff.imsave(str(train_path / 'non_upsampled_crops' / ('{0}_img.tif'.format(self.crop[2]))), self.crop[0])
        tiff.imsave(str(train_path / ('{0}_img_upsampled.tif'.format(self.crop[2]))), self.crop[1])
        self.get_crop()

    def select_crops_reject_button_clicked(self):
        self.select_crops_ids_denied.append(self.crop[2] + '_img_upsampled')
        self.get_crop()

    def create_train_data_annotate_button_clicked(self):

        # Check if training dir exists
        train_path = self.train_path_line_edit.text()
        if not QFileInfo(train_path).exists():
            self.output_text_edit.append('Training dir does not exist.')
            self.create_train_data.close()
            return

        # Check if training dir consists upsampled crops to annotate
        if len(list(Path(train_path).glob('*img_upsampled.tif'))) == 0:
            self.output_text_edit.append('Training dir contains no images to annotate.')
            self.create_train_data.close()
            return

        # Look for label tool executable
        if sys.platform == 'linux':
            if not QFileInfo(str(Path.cwd() / 'ImageLabelingTool_Ubuntu' / 'ImageLabelingTool.sh')).exists():
                self.output_text_edit.append('ImageLabelingTool not found.')
                self.create_train_data.close()
                return
        elif sys.platform == 'win32':
            if not QFileInfo(str(Path.cwd() / 'ImageLabelingTool_Windows' / 'ImageLabelingTool.exe')).exists():
                self.output_text_edit.append('ImageLabelingTool not found.')
                self.create_train_data.close()
                return
        else:
            self.output_text_edit.append('Your operating system is not supported by the labeling tool. Use '
                                          'another labeling tool and bring the data into the needed format (see '
                                          'the BeadNet repository readme).')
            self.create_train_data.close()
            return

        # Check if training dir contains image labeling tool json file
        if not QFileInfo(str(Path(train_path) / 'labeling.json')).exists():
            labeling_tool_settings = {'currImageIndex': 0,
                                      'imageDir': train_path,
                                      'labelingMode': False,
                                      'outputDir': train_path}

            with open(Path(train_path) / 'labeling.json', 'w', encoding='utf-8') as outfile:
                json.dump(labeling_tool_settings, outfile, ensure_ascii=False, indent=4)

        if sys.platform == 'linux':

            os.system(str(Path.cwd() / 'ImageLabelingTool_Ubuntu' / (
                    'ImageLabelingTool.sh -i ' + str(Path(train_path) / 'labeling.json'))))

            self.output_text_edit.append('If the labeling tool does not open, it may be that your used linux '
                                         'distribution is not supported by the tool. \n'
                                         'Try to install libxkbcommon-x11-0 and try again or run ImageLabelingTool.sh '
                                         'in a terminal to see the error message.')
        else:
            os.system(str(Path.cwd() / 'ImageLabelingTool_Windows' / (
                        'ImageLabelingTool.exe -i ' + str(Path(train_path) / 'labeling.json'))))

    def create_train_data_convert_button_clicked(self):

        # Check if training dir exists
        train_path = self.train_path_line_edit.text()
        if not QFileInfo(train_path).exists():
            self.output_text_edit.append('Training dir does not exist.')
            self.create_train_data.close()
            return

        # Check if training dir contains images/annotated images
        if len(list(Path(train_path).glob('*img_upsampled.tif'))) == 0 or len(
            list(Path(train_path).glob('*img_upsampled_label.tif'))) == 0:
            self.output_text_edit.append('No (annotated) images to convert found.')
            self.create_train_data.close()
            return

        # Check if training dir contains train/val/test
        if not QFileInfo(str(Path(train_path) / 'train')).exists():
            Path.mkdir(Path(train_path) / 'train', exist_ok=True)
        if not QFileInfo(str(Path(train_path) / 'val')).exists():
            Path.mkdir(Path(train_path) / 'val', exist_ok=True)
        if not QFileInfo(str(Path(train_path) / 'test')).exists():
            Path.mkdir(Path(train_path) / 'test', exist_ok=True)

        # Get list of images
        img_ids = list(Path(train_path).glob('*img_upsampled.tif'))

        # dataset_list
        dataset_list = []

        # Load image and label image
        for img_id in img_ids:

            # Check if img already used in train, val or test set
            if QFileInfo(str(img_id.parent / 'train' / img_id.name)).exists():
                self.output_text_edit.append(
                    'Image {} already used in train set. Skip'.format(img_id.name))
                continue
            if QFileInfo(str(img_id.parent / 'val' / img_id.name)).exists():
                self.output_text_edit.append(
                    'Image {} already used in val set. Skip'.format(img_id.name))
                continue
            if QFileInfo(str(img_id.parent / 'test' / img_id.name)).exists():
                self.output_text_edit.append(
                    'Image {} already used in test set. Skip'.format(img_id.name))
                continue

            # Load image
            img = tiff.imread(str(img_id))

            # Check if label image exists
            if not QFileInfo(str(Path(img_id.parent / (img_id.stem + '_label.tif')))).exists():
                self.output_text_edit.append(
                    'Image {} does not have an annotated label image. Skip'.format(img_id.name))
                continue
            seeds = tiff.imread(str(img_id.parent / (img_id.stem + '_label.tif'))) > 0

            # Check (annotated) image shape
            if len(img.shape) != 3 or img.shape[-1] != 1:
                if len(img.shape) == 2:
                    img = np.expand_dims(img, axis=-1)
                else:
                    self.output_text_edit.append(
                        'Image {} does not fulfill the shape requirements. Skip'.format(img_id.name))
                    continue
            if (len(seeds.shape) != 3 or seeds.shape[-1] != 1) and len(seeds.shape) != 2:
                self.output_text_edit.append(
                    'Label image {} does not fulfill the shape requirements. Skip'.format(img_id.stem + '_label.tif'))
                continue

            # Check if annotated image really contains a label (maybe not annotated yet)
            if np.sum(seeds) == 0:
                self.output_text_edit.append(
                    'Label image {} does not contain any label. Skip'.format(img_id.stem + '_label.tif'))
                continue

            if len(seeds.shape) == 3:  # Remove color chanel for the binary_dilation function
                seeds = seeds[:, :, 0]

            # Create dilated seed image
            seeds_dilation = binary_dilation(seeds, generate_binary_structure(2, 1))

            # Create metric seed image
            seeds_label = label(seeds, neighbors=8, background=0)
            seeds_metric = binary_dilation(seeds_dilation, generate_binary_structure(2, 2))
            seeds_metric = watershed(image=seeds_metric, markers=seeds_label, mask=seeds_metric, watershed_line=False)

            seeds = np.expand_dims(seeds, axis=-1)
            seeds_dilation = np.expand_dims(seeds_dilation, axis=-1)
            seeds_metric = np.expand_dims(seeds_metric, axis=-1)
            dataset_list.append([img_id.stem, img, seeds, seeds_dilation, seeds_metric])

        # Permute image indices and add to train/val/test set
        indices = np.random.permutation(len(dataset_list))
        train_indices = indices[0:int(np.floor(0.8 * len(indices)))]
        val_indices = train_indices[0:int(np.floor(0.25 * len(train_indices)))]
        train_indices = train_indices[int(np.floor(0.25 * len(train_indices))):]
        test_indices = indices[int(np.floor(0.8 * len(indices))):]

        for train_idx in train_indices:
            tiff.imsave(str(Path(train_path) / 'train' / (dataset_list[train_idx][0] + '.tif')),
                        dataset_list[train_idx][1])
            tiff.imsave(
                str(Path(train_path) / 'train' / (dataset_list[train_idx][0].split('img_upsampled')[0] + 'seeds.tif')),
                255 * dataset_list[train_idx][2].astype(np.uint8))
            tiff.imsave(str(Path(train_path) / 'train' / (
                        dataset_list[train_idx][0].split('img_upsampled')[0] + 'seeds_dilated.tif')),
                        255 * dataset_list[train_idx][3].astype(np.uint8))
            tiff.imsave(str(Path(train_path) / 'train' / (
                        dataset_list[train_idx][0].split('img_upsampled')[0] + 'seeds_metric.tif')),
                        dataset_list[train_idx][4])

        for val_idx in val_indices:
            tiff.imsave(str(Path(train_path) / 'val' / (dataset_list[val_idx][0] + '.tif')), dataset_list[val_idx][1])
            tiff.imsave(
                str(Path(train_path) / 'val' / (dataset_list[val_idx][0].split('img_upsampled')[0] + 'seeds.tif')),
                dataset_list[val_idx][2])
            tiff.imsave(str(
                Path(train_path) / 'val' / (dataset_list[val_idx][0].split('img_upsampled')[0] + 'seeds_dilated.tif')),
                        dataset_list[val_idx][3])
            tiff.imsave(str(
                Path(train_path) / 'val' / (dataset_list[val_idx][0].split('img_upsampled')[0] + 'seeds_metric.tif')),
                        dataset_list[val_idx][4])

        for test_idx in test_indices:
            tiff.imsave(str(Path(train_path) / 'test' / (dataset_list[test_idx][0] + '.tif')),
                        dataset_list[test_idx][1])
            tiff.imsave(
                str(Path(train_path) / 'test' / (dataset_list[test_idx][0].split('img_upsampled')[0] + 'seeds.tif')),
                dataset_list[test_idx][2])
            tiff.imsave(str(Path(train_path) / 'test' / (
                        dataset_list[test_idx][0].split('img_upsampled')[0] + 'seeds_dilated.tif')),
                        dataset_list[test_idx][3])
            tiff.imsave(str(
                Path(train_path) / 'test' / (dataset_list[test_idx][0].split('img_upsampled')[0] + 'seeds_metric.tif')),
                        dataset_list[test_idx][4])

        self.create_train_data.close()

    def help_shortcut_clicked(self):
        box = QMessageBox()
        box.setWindowTitle('Help')
        box.setText(
            'Paths:\n'
            '   Data dir: path to the images to analyze.\n'
            '   Model dir: path to the trained models / new models are saved here.\n'
            '   Training dir: path to a annotated data set (train, val, test).\n\n'
            'Settings:\n'
            '   Color channel: color channel to process / convert to grayscale.\n'
            '   Diameter range: approx. diameter in px of the objects to detect.\n'
            '   Device: device to use for training/evaluation/inference.\n'
            '   Ensemble mode: use multiple trained models for inference.\n\n'
            'Create new training data:\n'
            '   Create new crops: create crops to annotate from images in data dir.\n'
            '   Annotate: open image labeling tool.\n'
            '   Convert to data set: convert annotated data to a BeadNet data set.\n\n'
            'Train new models:\n'
            '   Batch size: set to <=2 if you have less than 12 GiB RAM/VRAM.\n'
            '   Epochs: number of epochs to train (reduce for quick tests).\n'
            '   Iterations: number of models to train (with & without dilation).\n'
            '   Train: train new models on the dataset in training dir.\n'
            '   Back: go back to main menu.\n\n'
            'Evaluate models: calculate metrics of all trained models on the test set.\n\n'
            'Inference: use best model(s) to evaluate the images in data dir.\n\n'
            'Press "ctrl + c" to stop training/evaluation/inference.\n\n'
            'Contact: tim.scherr@kit.edu')
        box.setPalette(get_dark_pallette())
        box.exec_()

    def train_button_clicked(self):

        # Check if training dir exists
        train_path = self.train_path_line_edit.text()
        if not QFileInfo(train_path).exists():
            self.output_text_edit.append('Training dir does not exist.')
            return

        # Check if train path contains train set
        if not QFileInfo(str(Path(train_path) / 'train')).exists():
            self.output_text_edit.append('Train set does not exist. Forgotten to convert data set?')
            return

        # Check if train path contains val set
        if not QFileInfo(str(Path(train_path) / 'val')).exists():
            self.output_text_edit.append('Val set does not exist.')
            return

        # Check if train set is not empty
        if len(list((Path(train_path) / 'train').glob('*.tif'))) == 0:
            self.output_text_edit.append('Train set is empty.')
            return

        # Check if train set contains labeled data
        if len(list((Path(train_path) / 'train').glob('*seeds.tif'))) == 0:
            self.output_text_edit.append('Train set contains no annotated images.')
            return

        # Check if train set contains dilated seeds data
        if len(list((Path(train_path) / 'train').glob('*seeds_dilated.tif'))) == 0:
            self.output_text_edit.append('Train set contains no dilated seed images.')
            return

        # Check if each train set image has a corresponding label image
        if len(list((Path(train_path) / 'train').glob('*seeds.tif'))) != len(
                list((Path(train_path) / 'train').glob('*upsampled.tif'))):
            self.output_text_edit.append('Not each train image has a corresponding seed image.')
            return

        # Check if each train set image has a corresponding dilated label image
        if len(list((Path(train_path) / 'train').glob('*seeds_dilated.tif'))) != len(
                list((Path(train_path) / 'train').glob('*upsampled.tif'))):
            self.output_text_edit.append('Not each train image has a corresponding dilated seed image.')
            return

        # Check if val set is not empty
        if len(list((Path(train_path) / 'val').glob('*.tif'))) == 0:
            self.output_text_edit.append('Val set is empty.')
            return

        # Check if train set contains labeled data
        if len(list((Path(train_path) / 'val').glob('*seeds.tif'))) == 0:
            self.output_text_edit.append('Val set contains no annotated images.')
            return

        # Check if train set contains dilated seeds data
        if len(list((Path(train_path) / 'val').glob('*seeds_dilated.tif'))) == 0:
            self.output_text_edit.append('Val set contains no dilated seed images.')
            return

        # Check if each train set image has a corresponding label image
        if len(list((Path(train_path) / 'val').glob('*seeds.tif'))) != len(
                list((Path(train_path) / 'val').glob('*upsampled.tif'))):
            self.output_text_edit.append('Not each val image has a corresponding seed image.')
            return

        # Check if each train set image has a corresponding label image
        if len(list((Path(train_path) / 'val').glob('*seeds_dilated.tif'))) != len(
                list((Path(train_path) / 'val').glob('*upsampled.tif'))):
            self.output_text_edit.append('Not each val image has a corresponding dilated seed image.')
            return

        # Check if model path exists
        model_path = self.model_path_line_edit.text()
        if not QFileInfo(model_path).exists():
            self.output_text_edit.append('Model dir does not exist. Path will be created.')

        self.train_settings.exec_()

    def train_settings_back_button_clicked(self):

        self.train_settings.close()

    def train_settings_train_button_clicked(self):

        self.stop_train = False
        self.train_settings.close()
        self.status_bar.showMessage('Busy')

        if self.device_gpu_checkbox.isChecked():
            self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
            if str(self.device) == 'cuda':
                torch.backends.cudnn.benchmark = True
            self.num_gpus = torch.cuda.device_count()
        else:
            self.device = torch.device("cpu")
            torch.backends.cudnn.benchmark = False
            self.num_gpus = 0

        train_path = self.train_path_line_edit.text()
        model_path = self.model_path_line_edit.text()

        # Make directory for the trained models
        Path(model_path).mkdir(exist_ok=True)

        for apply_dilation in [True, False]:  # Always train a model with and without dilated label seeds

            for i in range(int(self.train_settings_iterations_line_edit.text())):  # Train multiple models

                run_name = utils.unique_path(Path(model_path), 'beadnet_model_{:03d}.pth').stem

                train_configs = {'apply_dilation': apply_dilation,
                                 'batch_size': int(self.train_settings_batchsize_line_edit.text()),
                                 'break_condition': 30,
                                 'filters': [64, 128, 256, 512],
                                 'learning_rate': 6e-3,
                                 'max_epochs': int(self.train_settings_epochs_line_edit.text()),
                                 'num_gpus': self.num_gpus,
                                 'run_name': run_name,
                                 'path_models': Path(model_path)}

                net, net_status = build_unet(device=self.device,
                                             num_gpus=self.num_gpus,
                                             filters=train_configs['filters'],
                                             gui=self)

                if net_status == 1:
                    return

                # The training images are assumed to be uint16 crops of a min-max normalized image
                data_transforms = augmentors(augmentation='train', min_value=0, max_value=65535)

                train_configs['data_transforms'] = str(data_transforms)

                # Load training and validation set
                datasets = {x: BeadDataset(root_dir=Path(train_path),
                                           mode=x,
                                           transform=data_transforms[x],
                                           apply_dilation=train_configs['apply_dilation'])
                            for x in ['train', 'val']}

                # Train model
                QApplication.processEvents()
                train_status = train(net=net, datasets=datasets, configs=train_configs, device=self.device, gui=self)

                if train_status == 1:
                    self.status_bar.showMessage('Ready')
                    self.stop_train = False
                    return

                # Write information to txt-file
                utils.write_train_info(configs=train_configs)

        self.status_bar.showMessage('Ready')

    def eval_button_clicked(self):

        self.stop_train = False

        # Check if training dir exists
        train_path = self.train_path_line_edit.text()
        if not QFileInfo(train_path).exists():
            self.output_text_edit.append('Training dir does not exist.')
            return

        # Check if train path contains test set
        if not QFileInfo(str(Path(train_path) / 'test')).exists():
            self.output_text_edit.append('Test set does not exist.')
            return

        # Check if test set is not empty
        if len(list((Path(train_path) / 'test').glob('*.tif'))) == 0:
            self.output_text_edit.append('Test set is empty.')
            return

        # Check if test set contains labeled data
        if len(list((Path(train_path) / 'test').glob('*metric.tif'))) == 0:
            self.output_text_edit.append('Test set contains no metric images.')
            return

        # Check if each test set image has a corresponding label image
        if len(list((Path(train_path) / 'test').glob('*metric.tif'))) != len(
                list((Path(train_path) / 'test').glob('*upsampled.tif'))):
            self.output_text_edit.append('Not each image has a corresponding label image.')
            return

        # Check if model path exists
        model_path = self.model_path_line_edit.text()
        if not QFileInfo(model_path).exists():
            self.output_text_edit.append('Model dir does not exist.')
            return

        # Check if trained models exist
        if len(list(Path(model_path).glob('*.pth'))) == 0:
            self.output_text_edit.append('No models found in model dir.')
            return

        if self.device_gpu_checkbox.isChecked():
            self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
            if str(self.device) == 'cuda':
                torch.backends.cudnn.benchmark = True
            self.num_gpus = torch.cuda.device_count()
        else:
            self.device = torch.device("cpu")
            torch.backends.cudnn.benchmark = False
            self.num_gpus = 0

        self.status_bar.showMessage('Busy')

        # Get paths of all trained models
        models = sorted(Path(model_path).glob('*.pth'))

        # Dictionaries for the results
        model_metrics, model_metrics_bc = {}, {}

        self.output_text_edit.append('Evaluation of the {} found models:'.format(len(models)))

        # Evaluate all trained models
        QApplication.processEvents()
        for model in models:
            # Make directories for results on the test data set
            Path.mkdir(model.parent / model.stem, exist_ok=True)

            # Load training configs
            if not QFileInfo(str(model.parent / (model.stem + '.json'))).exists():
                self.output_text_edit.append('No json file for model {} found --> skip.'.format(model.stem))
                continue
            with open(model.parent / (model.stem + '.json')) as f:
                train_configs = json.load(f)

            dataset = BeadDataset(root_dir=Path(train_path),
                                  mode='test',
                                  transform=augmentors(augmentation='eval', min_value=0, max_value=65535))

            # Predict test images and calculate metrics
            results, eval_status = eval_model(model=model,
                                              num_gpus=self.num_gpus,
                                              filters=train_configs['filters'],
                                              dataset=dataset,
                                              device=self.device,
                                              gui=self)

            if eval_status == 1:

                self.stop_train = False
                self.status_bar.showMessage('Ready')
                return

            model_metrics[model.stem] = results['metrics']
            model_metrics_bc[model.stem] = results['metrics_border_corrected']

            # Append metrics to the training configs file
            utils.write_eval_info(model_path=model.parent, file_name=model.stem, results=results, mode='single_model')
            QApplication.processEvents()

        # Write file with metrics of all trained models
        utils.write_eval_info(model_path=model.parent, file_name='metrics', results=model_metrics, mode='all')
        utils.write_eval_info(model_path=model.parent, file_name='metrics_bc', results=model_metrics_bc, mode='all')

        self.status_bar.showMessage('Ready')
        self.output_text_edit.append('Evaluation finished.')

    def inference_button_clicked(self):

        self.stop_train = False

        # Check if data path exists
        data_path = self.data_path_line_edit.text()
        if not QFileInfo(data_path).exists():
            self.output_text_edit.append('Data dir does not exist.')
            return

        # Check if data path consists images (.png, .tif, .tiff, .jpg)
        # Get images to predict (.tif, .tiff, .png, .jpg)
        extensions = ['.tif*', '.png', '.jpg']
        data_path_files = []
        for extension in extensions:
            data_path_files.extend(list(Path(data_path).glob('*' + extension)))
        if len(list(data_path_files)) == 0:
            self.output_text_edit.append('No tiff/png/jpg files found in data dir.')
            return

        # Check if model path exists
        model_path = self.model_path_line_edit.text()
        if not QFileInfo(model_path).exists():
            self.output_text_edit.append('Model dir does not exist.')
            return

        # Check if trained models exist
        if len(list(Path(model_path).glob('*.pth'))) == 0:
            self.output_text_edit.append('No models found in model dir.')
            return

        # Check if models are evaluated
        if not QFileInfo(str(Path(model_path) / 'metrics_bc.json')).exists():
            self.output_text_edit.append('Evaluate models first.')
            return

        # Check if bead diameter is set
        if not (self.fourfold_upsampling_checkbox.isChecked() or self.twofold_upsampling_checkbox.isChecked() or self.no_upsampling_checkbox.isChecked()):
            self.output_text_edit.append('No diameter range specified.')
            return

        self.status_bar.showMessage('Busy')

        if self.fourfold_upsampling_checkbox.isChecked():
            self.output_text_edit.append('Very small beads, apply fourfold bilinear upsampling.')
        elif self.twofold_upsampling_checkbox.isChecked():
            self.output_text_edit.append('Small beads, apply twofold bilinear upsampling.')
        else:
            self.output_text_edit.append('Big beads, no upsampling needed.')
        self.output_text_edit.repaint()

        # Ensemble mode check
        use_ensemble = self.inference_checkbox.isChecked()

        # Find color channel
        if self.red_channel_checkbox.isChecked():
            color_channel = 'r'
        elif self.green_channel_checkbox.isChecked():
            color_channel = 'g'
        elif self.blue_channel_checkbox.isChecked():
            color_channel = 'b'
        else:
            color_channel = 'grayscale'

        if self.device_gpu_checkbox.isChecked():
            self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
            if str(self.device) == 'cuda':
                torch.backends.cudnn.benchmark = True
            self.num_gpus = torch.cuda.device_count()
        else:
            self.device = torch.device("cpu")
            torch.backends.cudnn.benchmark = False
            self.num_gpus = 0

        # Make result path
        Path.mkdir(Path(data_path) / 'results', exist_ok=True)
        # Find best model (best micro F-score)
        with open(Path(model_path) / 'metrics_bc.json') as f:
            metrics_bc = json.load(f)

        scores, models = [], []
        for model in metrics_bc:
            scores.append(metrics_bc[model]['Q_F_micro'])
            models.append(model)
        idcs = np.argsort(np.array(scores))
        # scores = [scores[i] for i in idcs]
        models = [models[i] for i in idcs]

        box = QMessageBox()
        box.setWindowTitle('Info')
        box.setText('The application window may freeze during inference of large images. That behaviour is normal.')
        box.setPalette(get_dark_pallette())
        box.exec_()

        if use_ensemble:
            # Use three best models for interference
            num_beads = {}
            for best_model in models[-3:]:
                self.output_text_edit.append('Inference (ensemble mode) using {}'.format(best_model))
                self.output_text_edit.repaint()
                model = Path(model_path) / (best_model + '.pth')
                if not QFileInfo(str(model.parent / (model.stem + '.json'))).exists():
                    self.output_text_edit.append('No json file for model {} found.')
                    self.status_bar.showMessage('Ready')
                    return
                with open(model.parent / (model.stem + '.json')) as f:
                    filters = json.load(f)['filters']

                QApplication.processEvents()
                if self.fourfold_upsampling_checkbox.isChecked():
                    upsampling_factor = 4
                elif self.twofold_upsampling_checkbox.isChecked():
                    upsampling_factor = 2
                else:
                    upsampling_factor = 1

                num_beads[best_model], inference_status = inference(data_path=Path(data_path),
                                                                    upsampling_factor=upsampling_factor,
                                                                    model=model,
                                                                    filters=filters,
                                                                    device=self.device,
                                                                    num_gpus=self.num_gpus,
                                                                    normalization=[None, None],
                                                                    color_channel=color_channel,
                                                                    gui=self)

                if inference_status == 1:
                    self.stop_train = False
                    self.status_bar.showMessage('Ready')
                    return

            # Rearrange the results
            results = {}
            for file in num_beads[best_model]:
                file_num_beads = []
                for model in num_beads:
                    file_num_beads.append(num_beads[model][file])
                results[file] = file_num_beads

            # Write result file with number of beads for each image
            utils.write_inference_results(results=results, path=Path(data_path) / 'results')

        else:

            # Use best model for inference
            best_model = models[-1]
            self.output_text_edit.append('Inference using the best model {}:'.format(best_model))
            self.output_text_edit.repaint()
            model = Path(model_path) / (best_model + '.pth')
            if not QFileInfo(str(model.parent / (model.stem + '.json'))).exists():
                self.output_text_edit.append('No json file for model {} found.')
                self.status_bar.showMessage('Ready')
                return
            with open(model.parent / (model.stem + '.json')) as f:
                filters = json.load(f)['filters']

            if self.fourfold_upsampling_checkbox.isChecked():
                upsampling_factor = 4
            elif self.twofold_upsampling_checkbox.isChecked():
                upsampling_factor = 2
            else:
                upsampling_factor = 1

            QApplication.processEvents()
            num_beads, inference_status = inference(data_path=Path(data_path),
                                                    upsampling_factor=upsampling_factor,
                                                    model=model,
                                                    filters=filters,
                                                    device=self.device,
                                                    num_gpus=self.num_gpus,
                                                    normalization=[None, None],
                                                    color_channel=color_channel,
                                                    gui=self)

            if inference_status == 1:
                self.stop_train = False
                self.status_bar.showMessage('Ready')
                return

            # Write result file with number of beads for each image
            utils.write_inference_results(results=num_beads, path=Path(data_path) / 'results')

        self.output_text_edit.append('Inference finished.')

        webbrowser.open(str(Path(data_path) / 'results' / 'results.json'))

        if sys.platform == 'win32':
            os.startfile(Path(data_path) / 'results')
        elif sys.platform == 'darwin':
            subprocess.Popen(["open", str(Path(data_path) / 'results')])
        elif sys.platform == 'linux':
            subprocess.Popen(["xdg-open", str(Path(data_path) / 'results')])

        self.status_bar.showMessage('Ready')

    def stop_calculation(self):

        self.stop_train = True


def run_gui(model_path, training_data_path, device, num_gpus):
    app = QApplication([])
    app.setStyle('Fusion')

    window = BeadNetMainWindow(model_path=model_path,
                               train_path=training_data_path,
                               device=device,
                               num_gpus=num_gpus)
    app.exec_()


def get_dark_pallette():
    dark_palette = QPalette()
    dark_palette.setColor(QPalette.Window, QColor(53, 53, 53))
    dark_palette.setColor(QPalette.WindowText, Qt.white)
    dark_palette.setColor(QPalette.Base, QColor(25, 25, 25))
    dark_palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
    dark_palette.setColor(QPalette.ToolTipBase, Qt.white)
    dark_palette.setColor(QPalette.ToolTipText, Qt.white)
    dark_palette.setColor(QPalette.Text, Qt.white)
    dark_palette.setColor(QPalette.Button, QColor(53, 53, 53))
    dark_palette.setColor(QPalette.ButtonText, Qt.white)
    dark_palette.setColor(QPalette.BrightText, Qt.red)
    dark_palette.setColor(QPalette.Link, QColor(42, 130, 218))
    dark_palette.setColor(QPalette.Highlight, QColor(42, 130, 218))
    dark_palette.setColor(QPalette.HighlightedText, Qt.black)

    return dark_palette

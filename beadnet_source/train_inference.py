import gc
import numpy as np
import slidingwindow as sw
import tifffile as tiff
import time
import torch
import torch.optim as optim
import warnings

from psutil import virtual_memory
from PyQt5.QtWidgets import QApplication
from skimage.io import imread
from skimage.transform import rescale
from torch.optim.lr_scheduler import ReduceLROnPlateau
from beadnet_source.losses import get_loss
from beadnet_source.metrics import calc_metrics, metric_scores
from beadnet_source.postprocessing import seed_detection
from beadnet_source.unets import build_unet
from beadnet_source import utils


def train(net, datasets, configs, device, gui=None):
    """ Train the model.

    :param net: Model/Network to train.
        :type net:
    :param datasets: Dictionary containing the training and the validation data set.
        :type datasets: dict
    :param configs: Dictionary with configurations of the training process.
        :type configs: dict
    :param device: Use (multiple) GPUs or CPU.
        :type device: torch device
    :param gui: Graphical user interface if used.
        :type gui: QWidget
    :return: Exit status.
    """

    if gui is not None:
        gui.output_text_edit.append('-' * 20)
        gui.output_text_edit.append('Train {0} on {1} images, validate on {2} images'.format(configs['run_name'],
                                                                                             len(datasets['train']),
                                                                                             len(datasets['val'])))
        gui.output_text_edit.repaint()
    else:
        print('-' * 20)
        print('Train {0} on {1} images, validate on {2} images'.format(configs['run_name'],
                                                                       len(datasets['train']),
                                                                       len(datasets['val'])))

    # Data loader for training and validation set
    apply_shuffling = {'train': True, 'val': False}
    dataloader = {x: torch.utils.data.DataLoader(datasets[x],
                                                 batch_size=configs['batch_size'],
                                                 shuffle=apply_shuffling,
                                                 pin_memory=True,
                                                 num_workers=0)
                  for x in ['train', 'val']}

    # Loss function and optimizer
    criterion = get_loss('bce_dice')
    optimizer = optim.Adam(net.parameters(),
                           lr=configs['learning_rate'],
                           betas=(0.9, 0.999),
                           eps=1e-08,
                           amsgrad=True)

    # Learning rate scheduler
    scheduler = ReduceLROnPlateau(optimizer, mode='min', factor=0.25, patience=13, verbose=True, min_lr=6e-5)

    # Auxiliary variables for training process
    epochs_without_improvement, train_loss, val_loss, best_loss, since = 0, [], [], 1e4, time.time()

    try:
        # Training process
        for epoch in range(configs['max_epochs']):

            if gui is not None:
                gui.output_text_edit.append('-' * 10)
                gui.output_text_edit.append('Epoch {}/{}'.format(epoch + 1, configs['max_epochs']))
                gui.output_text_edit.append('-' * 10)
                gui.output_text_edit.repaint()
                gui.update()
                i = 0
            else:
                print('-' * 10)
                print('Epoch {}/{}'.format(epoch + 1, configs['max_epochs']))
                print('-' * 10)

            start = time.time()

            # Each epoch has a training and validation phase
            for phase in ['train', 'val']:
                if phase == 'train':
                    net.train()  # Set model to training mode
                else:
                    net.eval()  # Set model to evaluation mode

                running_loss = 0.0

                # Iterate over data
                for samples in dataloader[phase]:

                    if gui is not None:  # Avoid freezing of the gui
                        if device.type == 'cpu':
                            QApplication.processEvents()
                            if gui.stop_train:
                                gui.output_text_edit.append('Stop training process.')
                                return 1
                        else:
                            if i % 10 == 0:
                                if gui.stop_train:
                                    gui.output_text_edit.append('Stop training process.')
                                    return 1
                                QApplication.processEvents()
                        i += 1

                    # Get inputs and labels and put them on GPU if available
                    inputs, labels, _ = samples
                    inputs = inputs.to(device)
                    labels = labels.to(device)

                    # Zero the parameter gradients
                    optimizer.zero_grad()

                    # Forward pass (track history if only in train)
                    with torch.set_grad_enabled(phase == 'train'):
                        outputs = net(inputs)
                        loss = criterion(outputs, labels)

                        # Backward (optimize only if in training phase)
                        if phase == 'train':
                            loss.backward()
                            optimizer.step()

                    # Statistics
                    running_loss += loss.item() * inputs.size(0)

                epoch_loss = running_loss / len(datasets[phase])

                if phase == 'train':
                    train_loss.append(epoch_loss)
                    if gui is not None:
                        gui.output_text_edit.append('Training loss: {:.4f}'.format(epoch_loss))
                        gui.output_text_edit.repaint()
                    else:
                        print('Training loss: {:.4f}'.format(epoch_loss))
                else:
                    val_loss.append(epoch_loss)
                    if gui is not None:
                        gui.output_text_edit.append('Validation loss: {:.4f}'.format(epoch_loss))
                        gui.output_text_edit.repaint()
                    else:
                        print('Validation loss: {:.4f}'.format(epoch_loss))

                    scheduler.step(epoch_loss)

                    if epoch_loss < best_loss:
                        if gui is not None:
                            gui.output_text_edit.append(
                                'Validation loss improved from {:.4f} to {:.4f}. Save model.'.format(best_loss, epoch_loss))
                            gui.output_text_edit.repaint()
                            QApplication.processEvents()
                        else:
                            print(
                                'Validation loss improved from {:.4f} to {:.4f}. Save model.'.format(best_loss, epoch_loss))
                        best_loss = epoch_loss

                        # The state dict of data parallel (multi GPU) models need to get saved in a way that allows to
                        # load them also on single GPU or CPU
                        if configs['num_gpus'] > 1:
                            torch.save(net.module.state_dict(), str(configs['path_models'] / (configs['run_name'] + '.pth')))
                        else:
                            torch.save(net.state_dict(), str(configs['path_models'] / (configs['run_name'] + '.pth')))
                        epochs_without_improvement = 0
                    else:
                        if gui is not None:
                            gui.output_text_edit.append('Validation loss did not improve.')
                            gui.output_text_edit.repaint()
                        else:
                            print('Validation loss did not improve.')
                        epochs_without_improvement += 1

                if gui is not None:
                    QApplication.processEvents()

            # Epoch training time
            if gui is not None:
                gui.output_text_edit.append('Epoch training time: {:.1f}s'.format(time.time() - start))
                gui.output_text_edit.repaint()
            else:
                print('Epoch training time: {:.1f}s'.format(time.time() - start))

            # Break training if plateau is reached
            if epochs_without_improvement == configs['break_condition']:
                if gui is not None:
                    gui.output_text_edit.append(
                        str(epochs_without_improvement) + ' epochs without validation loss improvement --> break')
                    gui.output_text_edit.repaint()
                else:
                    print(str(epochs_without_improvement) + ' epochs without validation loss improvement --> break')
                break

        # Total training time
        time_elapsed = time.time() - since
        if gui is not None:
            gui.output_text_edit.append(
                'Training complete in {:.0f}min {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
            gui.output_text_edit.append('-' * 20)
            gui.output_text_edit.repaint()
        else:
            print('Training complete in {:.0f}min {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
            print('-' * 20)
        configs['training_time'], configs['trained_epochs'] = time_elapsed, epoch + 1

        # Save loss
        stats = np.transpose(np.array([list(range(1, len(train_loss) + 1)), train_loss, val_loss]))
        np.savetxt(fname=str(configs['path_models'] / (configs['run_name'] + '_loss.txt')), X=stats,
                   fmt=['%3i', '%2.5f', '%2.5f'],
                   header='Epoch, training loss, validation loss', delimiter=',')
    except:
        if gui is not None:
            gui.output_text_edit.append('Stop training process. Possible reasons could be too less RAM/VRAM.\n'
                                        'Reduce the batch size and try again.')
            return 1

    # Clear memory
    del net
    gc.collect()

    return 0


def eval_model(model, num_gpus, filters, dataset, device, gui=None):
    """ Evaluate the model (calculate metrics on test data set).

    :param model: Path to the state dictionary of a trained model.
        :type model: pathlib path object.
    :param num_gpus: Number of GPUs to use.
        :type num_gpus: int
    :param filters: Number of feature maps in the encoder of the trained model.
        :type filters: list
    :param dataset: Test data set.
        :type dataset
    :param device: Use GPU or CPU.
        :type device: torch device
    :param gui: Graphical user interface if used.
        :type gui: QWidget
    :return: Metrics on the test data set, exit status.
    """

    # Load trained model and set it to eval mode
    if gui is not None:
        gui.output_text_edit.append('... evaluate model {0} on the {1} test images ...'.format(model.stem, len(dataset)))
        gui.output_text_edit.repaint()
    else:
        print('Evaluate model {0} on the {1} test images'.format(model.stem, len(dataset)))
    net, net_status = build_unet(device=device, num_gpus=num_gpus, filters=filters, gui=gui)

    if net_status == 1:
        return

    if num_gpus > 1:
        net.module.load_state_dict(torch.load(str(model), map_location=device))
    else:
        net.load_state_dict(torch.load(str(model), map_location=device))
    net.eval()
    torch.set_grad_enabled(False)

    # Set up data loader for the data set
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=1, shuffle=False, pin_memory=True, num_workers=0)

    # Auxiliary variables for the evaluation process.
    eval_time, metrics, metrics_bc = [], [], []

    # Prediction process (iterate over data)
    for sample in dataloader:

        if gui is not None:  # Avoid freezing of the gui
            QApplication.processEvents()
            if gui.stop_train:
                gui.output_text_edit.append('Stop evaluation.')
                return 1, 1

        since = time.time()

        # Get inputs and labels and put them on GPU if available. Remove the batch dimension for the label
        img, label, img_id = sample
        img = img.to(device)
        img_id = img_id[0]
        label = label.numpy()[0, :, :, :].astype(np.uint16)

        # Predict test image
        prediction = net(img)
        prediction = torch.sigmoid(prediction)
        prediction = prediction.cpu().numpy()

        # Post-processing
        _, bead_seeds, _ = seed_detection(prediction)

        eval_time.append(time.time() - since)

        # Calculate scores needed for the final metrics
        metrics.append(metric_scores(prediction=bead_seeds, ground_truth=label))

        # Border corrected metrics
        bead_seeds_bc = utils.border_correction(bead_seeds, label)
        metrics_bc.append(metric_scores(prediction=bead_seeds_bc, ground_truth=label))

        # Save predictions
        # tiff.imsave(str(model.parent / model.stem / (img_id + '_prediction.tif')), prediction)
        tiff.imsave(str(model.parent / model.stem / (img_id + '_bead_seeds.tif')), bead_seeds)
        tiff.imsave(str(model.parent / model.stem / (img_id + '_bead_seeds_overlay.tif')),
                    np.maximum(125 * (label > 0).astype(np.uint8), 255 * bead_seeds.astype(np.uint8)))
        tiff.imsave(str(model.parent / model.stem / (img_id + '_bead_seeds_bc.tif')), bead_seeds_bc)
        tiff.imsave(str(model.parent / model.stem / (img_id + '_bead_seeds_bc_overlay.tif')),
                    np.maximum(125 * (label > 0).astype(np.uint8), 255 * bead_seeds_bc.astype(np.uint8)))

    eval_time = np.mean(np.array(eval_time))

    # Calculate micro and macro metrics with and without border correction
    metrics = calc_metrics(metrics)
    metrics_bc = calc_metrics(metrics_bc)

    # Clear memory
    del net
    torch.cuda.empty_cache()
    gc.collect()

    results = {'eval_time': [eval_time, str(device)], 'metrics': metrics, 'metrics_border_corrected': metrics_bc}

    return results, 0


def inference(data_path, upsampling_factor, model, filters, device, num_gpus, normalization, color_channel, gui=None):
    """ Predict images (only tif, jpg and png images are supported).

    :param data_path: Path to the data to evaluate
        :type data_path: pathlib path object.
    :param upsampling_factor: Upsampling factor.
        :type upsampling_factor: int
    :param model: Path to the state dictionary of a trained model.
        :type model: pathlib path object.
    :param filters: Number of feature maps in the encoder of the trained model.
        :type filters: list
    :param device: Use GPU or CPU.
        :type device: torch device
    :param num_gpus: Number of GPUs to use.
        :type num_gpus: int
    :param normalization: Minimum/maximum value for the normalization of an image. All values below/above are clipped.
        :type normalization: list
    :param color_channel: Color channel to process.
        :type color_channel: str
    :param gui: Graphical user interface if used.
        :type gui: QWidget
    :return: Dictionary containing the number of beads for each image in the data_path, exit status.
    """

    num_beads = {}

    # Load trained model and set it to eval mode
    net, net_status = build_unet(device=device, num_gpus=num_gpus, filters=filters, gui=gui)

    if net_status == 1:
        return

    if num_gpus > 1:
        net.module.load_state_dict(torch.load(str(model), map_location=device))
    else:
        net.load_state_dict(torch.load(str(model), map_location=device))
    net.eval()
    torch.set_grad_enabled(False)

    # Get images to predict (.tif, .tiff, .png, .jpg)
    extensions = ['.tif*', '.png', '.jpg']
    files = []
    for extension in extensions:
        files.extend(list(data_path.glob('*' + extension)))

    # Get RAM in GiB to decide later if a sliding window approach is needed.
    if num_gpus > 0:
        mem = torch.cuda.get_device_properties(0).total_memory / 10**9 / 1.07374 * num_gpus
    else:
        mem = virtual_memory().total / 10**9 / 1.07374

    # Prediction process (iterate over images/files)
    for i, file in enumerate(files):

        if gui is not None:
            QApplication.processEvents()
            if gui.stop_train:
                gui.output_text_edit.append('Stop inference.')
                return 1, 1

        # Load image
        if file.suffix == '.tif' or file.suffix == '.tiff':
            img = tiff.imread(str(file))
        else:  # jpg/png
            img = imread(str(file))

        # Get position of the color channel
        if len(img.shape) == 2:  # grayscale image, add pseudo color channel
            img = np.expand_dims(img, axis=-1)
        elif len(img.shape) == 3:  # rgb image
            if img.shape[-1] == 3:  # rgb image with channels last
                if color_channel == 'r':
                    img = np.expand_dims(img[:, :, 0], axis=-1)
                elif color_channel == 'g':
                    img = np.expand_dims(img[:, :, 1], axis=-1)
                elif color_channel == 'b':
                    img = np.expand_dims(img[:, :, 2], axis=-1)
                elif color_channel == 'grayscale':
                    img = np.expand_dims(np.mean(img, axis=-1), axis=-1)
            elif img.shape[0] == 3:  # rgb image with channels first
                if color_channel == 'r':
                    img = np.expand_dims(img[0, :, :], axis=-1)
                elif color_channel == 'g':
                    img = np.expand_dims(img[1, :, :], axis=-1)
                elif color_channel == 'b':
                    img = np.expand_dims(img[2, :, :], axis=-1)
                elif color_channel == 'grayscale':
                    img = np.expand_dims(np.mean(img, axis=0), axis=-1)
            elif img.shape[0] == 1:
                    img = np.swapaxes(img, 0, -1)
            elif img.shape[-1] == 1:
                pass
            else:
                if gui is not None:
                    gui.output_text_edit.append(
                        '... processing {0}{1}: unsupported channel.'.format(file.stem, file.suffix))
                    gui.output_text_edit.repaint()
                    continue
        else:
            if gui is not None:
                gui.output_text_edit.append(
                    '... processing {0}{1}: unsupported shape. Supported are (h,w), (h,w,c), (c,h,w).'.format(
                        file.stem, file.suffix))
                gui.output_text_edit.repaint()
                continue

        # Min-max normalize the image to [0, 65535] (uint16 range)
        img_normalized = 65535/2 * (utils.min_max_normalization(img, normalization[0], normalization[1]) + 1)
        img_normalized = img_normalized.astype(np.uint16)

        # Select the applied upsampling dependent from the bead diameter
        if upsampling_factor == 4:
            print('Very small beads, apply fourfold bilinear upsampling.') if i == 0 else None
            img_upsampled = rescale(img_normalized, 4, order=2, multichannel=True)
            img_upsampled = np.clip(65535 * img_upsampled, 0, 65535).astype(np.uint16)
        elif upsampling_factor == 2:
            print('Small beads, apply twofold bilinear upsampling.') if i == 0 else None
            img_upsampled = rescale(img_normalized, 2, order=2, multichannel=True)
            img_upsampled = np.clip(65535 * img_upsampled, 0, 65535).astype(np.uint16)
        elif upsampling_factor == 1:
            print('Big beads, no upsampling needed.') if i == 0 else None
            img_upsampled = img_normalized
        else:
            print('Select an upsampling factor of 1, 2 or 4!')
            continue

        # Check if sliding window approach is needed
        if mem < 8:
            use_sliding_window = True
            window_size = 256
        elif img_upsampled.shape[0] > 1024:
            use_sliding_window = True
            window_size = 1024
        elif img_upsampled.shape[1] > 1024:
            use_sliding_window = True
            window_size = 1024
        else:
            use_sliding_window = False

        # Check if zero-padding is needed
        if use_sliding_window:
            pads = [0, 0]
            if img_upsampled.shape[0] < window_size:
                window_pads = window_size - img_upsampled.shape[0]
                img_upsampled = np.pad(img_upsampled, ((window_pads, 0), (0, 0), (0, 0)), mode='constant')
                pads[0] = window_pads
            if img_upsampled.shape[1] < window_size:
                window_pads = window_size - img_upsampled.shape[1]
                img_upsampled = np.pad(img_upsampled, ((0, 0), (window_pads, 0), (0, 0)), mode='constant')
                pads[1] = window_pads
        else:
            img_upsampled, pads, status = utils.zero_pad_model_input(img_upsampled=img_upsampled, gui=gui)
            if status == 1:
                return 1, 1

        # First normalize image into the range [-1, 1] to get a zero-centered input for the network
        net_input = utils.min_max_normalization(img=img_upsampled, min_value=0, max_value=65535)

        # Bring input into the shape [batch, channel, height, width]
        net_input = np.transpose(np.expand_dims(net_input, axis=0), [0, 3, 1, 2])

        # Prediction
        if use_sliding_window:
            if gui is not None:
                gui.output_text_edit.append('... processing {0}{1} ...'.format(file.stem, file.suffix))
                gui.output_text_edit.repaint()
            # Preallocate array to combine the sliding window predictions
            prediction = np.zeros(shape=net_input.shape, dtype=net_input.dtype)
            # Remove the batch dimension to generate the sliding windows
            net_input = net_input[0]
            # Generate sliding windows with overlap
            windows = sw.generate(data=net_input,
                                  dimOrder=sw.DimOrder.ChannelHeightWidth,
                                  maxWindowSize=window_size,
                                  overlapPercent=0.1)
            for window in windows:
                net_input_window = np.expand_dims(net_input[window.indices()], axis=0)
                # Don't use pixels near boundary of a (inner) sliding window
                h = 12 if window.indices()[1].start > 0 else 0
                w = 12 if window.indices()[2].start > 0 else 0
                hmin = window.indices()[1].start + h
                wmin = window.indices()[2].start + w
                net_input_window = torch.from_numpy(net_input_window).to(device)
                prediction_window = torch.sigmoid(net(net_input_window)).cpu().numpy()
                # Combine the sliding windows
                prediction[:, :, hmin:window.indices()[1].stop, wmin:window.indices()[2].stop] = prediction_window[:, :, h:, w:]

                if gui is not None:  # Avoid freezing of the gui
                    QApplication.processEvents()
        else:
            if gui is not None:
                gui.output_text_edit.append('... processing {0}{1} ...'.format(file.stem, file.suffix))
                gui.output_text_edit.repaint()
            net_input = torch.from_numpy(net_input)
            prediction = torch.sigmoid(net(net_input.to(device))).cpu().numpy()

        # Post-processing
        _, bead_seeds, num_beads[file.stem] = seed_detection(prediction)

        bead_seeds = bead_seeds[pads[0]:, pads[1]:, :]
        img_upsampled = img_upsampled[pads[0]:, pads[1]:, :]

        # Save predictions
        bead_seeds_overlay = utils.generate_bead_overlay(img_upsampled, bead_seeds)
        tiff.imsave(str(data_path / 'results' / (file.stem + '_upsampled.tif')), img_upsampled)
        tiff.imsave(str(data_path / 'results' / (file.stem + '_{}_seeds.tif'.format(str(model.stem)))),
                    255 * bead_seeds.astype(np.uint8))
        tiff.imsave(str(data_path / 'results' / (file.stem + '_{}_seeds_overlay.tif'.format(str(model.stem)))),
                    bead_seeds_overlay)

        if gui is not None:
            QApplication.processEvents()

    return num_beads, 0

import json
import numpy as np
from scipy.ndimage import binary_dilation
from scipy.ndimage.morphology import generate_binary_structure


def border_correction(prediction_bead_seeds, ground_truth):
    """ Minimize the influence of only partially visible beads on the borders onto the evaluation.

    :param prediction_bead_seeds: Predicted bead seeds.
        :type prediction_bead_seeds:
    :param ground_truth: Intensity-coded label image.
        :type
    :return: Border corrected prediction.
    """

    border_size = 7

    # Apply border correction
    border_area = ground_truth.copy() > 0
    border_area[border_size:border_area.shape[0] - border_size, border_size:border_area.shape[1] - border_size] = 1
    prediction_bead_seeds_bc = prediction_bead_seeds * border_area

    return prediction_bead_seeds_bc


def generate_bead_overlay(img, bead_seeds):
    """ Image-bead_prediction overlay.

    :param img: Image to predict.
        :type img:
    :param bead_seeds: Predicted bead seeds.
        :type bead_seeds:
    :return: Overlay (img: gray, beads: red)
    """

    bead_seeds = np.expand_dims(binary_dilation(bead_seeds[:, :, 0], generate_binary_structure(2, 1)) > 0, axis=-1)
    bead_seeds_rgb = np.tile(bead_seeds, (1, 1, 3))
    bead_seeds_rgb[:, :, 1] = 0
    bead_seeds_rgb[:, :, 2] = 0
    img_rgb = np.tile(img, (1, 1, 3))
    bead_seeds_overlay = (255 * img_rgb.astype(np.float32) / 65535).astype(np.uint8)
    bead_seeds_overlay[bead_seeds_rgb] = 255
    bead_seeds_overlay[bead_seeds_rgb[:, :, [2, 0, 1]]] = 0
    bead_seeds_overlay[bead_seeds_rgb[:, :, [2, 1, 0]]] = 0

    return bead_seeds_overlay


def get_bead_ids(img):
    """ Get bead ids in intensity-coded label image.

    :param img: Intensity-coded bead detection.
        :type:
    :return: List of bead ids.
    """

    values = np.unique(img)
    values = values[values > 0]

    return values


def min_max_normalization(img, min_value=None, max_value=None):
    """ Min-max-normalization for images.

    :param img: Image with shape  [height, width, color channels].
        :type img:
    :param min_value: Minimum value for the normalization. All values below this value are clipped
        :type min_value: int
    :param max_value: Maximum value for the normalization. All values above this value are clipped.
        :type max_value: int
    :return: Normalized image (float32)
    """

    if max_value is None:

        max_value = img.max()

    if min_value is None:  # Get new minimum value

        min_value = img.min()

    # Clip image to filter hot and cold pixels
    img = np.clip(img, min_value, max_value)

    # Apply Min-max-normalization
    img = 2 * (img.astype(np.float32) - min_value) / (max_value - min_value) - 1

    return img.astype(np.float32)


def unique_path(directory, name_pattern):
    """ Get unique file name to save trained model.

    :param directory: Path to the model directory
        :type directory: pathlib path object.
    :param name_pattern: Pattern for the file name
        :type name_pattern: str
    :return:
    """
    counter = 0
    while True:
        counter += 1
        path = directory / name_pattern.format(counter)
        if not path.exists():
            return path


def write_train_info(configs):
    """ Write training configurations into a json file.

    :param configs: Dictionary with configurations of the training process.
        :type configs: dict
    :return: None
    """

    with open(configs['path_models'] / (configs['run_name'] + '.json'), 'w', encoding='utf-8') as outfile:
        del configs['path_models']
        json.dump(configs, outfile, ensure_ascii=False, indent=2)

    return None


def write_eval_info(results, model_path, file_name, mode='single_model'):
    """ Write evaluation results into a json file.

    :param results: Evaluation results.
        :type results: dict
    :param model_path: Path to the model used.
        :type model_path: pathlib path object
    :param file_name: Name of the file to write.
        :type file_name: str
    :param mode: Write results of single model or multiple models.
        :type mode: str
    :return: None
    """
    if mode == 'single_model':  # load file and append info

        with open(model_path / (file_name + '.json')) as infile:
            infile_configs = json.load(infile)

            infile_configs['eval_time'] = results['eval_time']
            infile_configs['metrics'] = results['metrics']
            infile_configs['metrics_border_corrected'] = results['metrics_border_corrected']

        with open(model_path / (file_name + '.json'), 'w', encoding='utf-8') as outfile:
            json.dump(infile_configs, outfile, ensure_ascii=False, indent=2)

    else:
        with open(model_path / (file_name + '.json'), 'w', encoding='utf-8') as outfile:
            json.dump(results, outfile, ensure_ascii=False, indent=2)

    return None


def write_inference_results(results, path):
    """ Write inference results (number of beads) into a json file.

    :param results: Inference results.
        :type results: dict
    :param path: Result path
        :type path: pathlib path object.
    :return: None
    """

    with open(path / 'results.json', 'w', encoding='utf-8') as outfile:
        json.dump(results, outfile, ensure_ascii=False, indent=2)


def zero_pad_model_input(img_upsampled, gui=None):
    """ Zero-pad model input to get for the model needed sizes.

    :param img_upsampled:
        :type img_upsampled:
    :param gui:
        :type gui:
    :return: zero-padded img, [0s padded in y-direction, 0s padded in x-direction], exit status
    """

    pads = []
    
    for i in range(0, 2):  # 0: y-pads, 1: x-pads

        if img_upsampled.shape[i] < 128:
            # Zero-pad to 128
            pads.append(128 - img_upsampled.shape[i])

        elif img_upsampled.shape[i] == 128:
            # No zero-padding needed
            pads.append(0)

        elif 128 < img_upsampled.shape[i] < 256:
            # Zero-pad to 256
            pads.append(256 - img_upsampled.shape[i])

        elif img_upsampled.shape[i] == 256:
            # No zero-padding needed
            pads.append(0)

        elif 256 < img_upsampled.shape[i] < 512:
            # Zero-pad to 512
            pads.append(512 - img_upsampled.shape[i])

        elif img_upsampled.shape[i] == 512:
            # No zero-padding needed
            pads.append(0)

        elif 512 < img_upsampled.shape[i] < 768:
            # Zero-pad to 768
            pads.append(768 - img_upsampled.shape[i])

        elif img_upsampled.shape[i] == 768:
            # No zero-padding needed
            pads.append(0)

        elif 768 < img_upsampled.shape[i] < 1024:
            # Zero-pad to 1024
            pads.append(1024 - img_upsampled.shape[i])

        elif img_upsampled.shape[i] == 1024:
            # No zero-padding needed
            pads.append(0)

        elif 1024 < img_upsampled.shape[i] < 1360:
            # Zero-pad to 1280
            pads.append(1360 - img_upsampled.shape[i])

        elif img_upsampled.shape[i] == 1360:
            # No zero-padding needed
            pads.append(0)

        elif 1360 < img_upsampled.shape[i] < 1680:
            # Zero-pad to 1680
            pads.append(1680 - img_upsampled.shape[i])

        elif img_upsampled.shape[i] == 1680:
            # No zero-padding needed
            pads.append(0)

        elif 1680 < img_upsampled.shape[i] < 2048:
            # Zero-pad to 2048
            pads.append(2048 - img_upsampled.shape[i])

        elif img_upsampled.shape[i] == 2048:
            # No zero-padding needed
            pads.append(0)

        elif 2048 < img_upsampled.shape[i] < 2560:
            # Zero-pad to 2560
            pads.append(2560 - img_upsampled.shape[i])

        elif img_upsampled.shape[i] == 2560:
            # No zero-padding needed
            pads.append(0)

        elif 2560 < img_upsampled.shape[i] < 4096:
            # Zero-pad to 4096
            pads.append(4096 - img_upsampled.shape[i])

        elif img_upsampled.shape[i] == 4096:
            # No zero-padding needed
            pads.append(0)

        else:
            if gui is not None:
                gui.output_text_edit.append('Cut the image into smaller pieces.')
            return 1, 1, 1

    img_upsampled = np.pad(img_upsampled, ((pads[0], 0), (pads[1], 0), (0, 0)), mode='constant')

    return img_upsampled, pads, 0

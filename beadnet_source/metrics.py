import numpy as np
from itertools import chain
from skimage import measure

from beadnet_source.utils import get_bead_ids


def calc_metrics(metric_scores_list):
    """ Calculate micro and macro metrics out of the metric scores of all test images.

    :param metric_scores_dict: Metric scores for all test images
        :type metric_scores_list: list
    :return: metrics
    """

    N_split, N_miss, N_add, Q_P, Q_R, Q_F, N_gt, N_pred = [], [], [], [], [], [], [], []
    tp, fp, fn = [], [], []

    for score in metric_scores_list:
        N_split.append(score['N_split']), N_miss.append(score['N_miss']), N_add.append(score['N_add'])
        Q_P.append(score['Q_P']), Q_R.append(score['Q_R']), Q_F.append(score['Q_F'])
        N_gt.append(score['N_gt']), N_pred.append(score['N_pred'])
        tp.append(score['tp']), fp.append(score['fp']), fn.append(score['fn'])

    N_split, N_miss, N_add = np.array(N_split), np.array(N_miss), np.array(N_add)
    N_gt, N_pred = np.array(N_gt), np.array(N_pred)
    tp, fp, fn = np.array(tp), np.array(fp), np.array(fn)
    Q_P_macro, Q_R_macro, Q_F_macro = np.mean(np.array(Q_P)), np.mean(np.array(Q_R)), np.mean(np.array(Q_F))
    Q_P_micro = np.sum(tp) / (np.sum(tp) + np.sum(fp)) if (np.sum(tp) + np.sum(fp)) > 0 else 0
    Q_R_micro = np.sum(tp) / (np.sum(tp) + np.sum(fn)) if (np.sum(tp) + np.sum(fn)) > 0 else 0

    metrics = {
        'Q_split_micro': float(np.sum(N_split) / np.sum(N_gt)),
        'Q_split_macro': float(np.mean(N_split / N_gt)),
        'Q_miss_micro': float(np.sum(N_miss) / np.sum(N_gt)),
        'Q_miss_macro': float(np.mean(N_miss / N_gt)),
        'Q_add_micro': float(np.sum(N_add) / np.sum(N_gt)),
        'Q_add_macro': float(np.mean(N_add / N_gt)),
        'N_gt': int(np.sum(N_gt)),
        'N_pred': int(np.sum(N_pred)),
        'Q_P_micro': float(Q_P_micro),
        'Q_P_macro': float(Q_P_macro),
        'Q_R_micro': float(Q_R_micro),
        'Q_R_macro': float(Q_R_macro),
        'Q_F_macro': float(Q_F_macro),
        'Q_F_micro': float(2 * Q_P_micro * Q_R_micro / (Q_P_micro + Q_R_micro)) if (Q_P_micro + Q_R_micro) > 0 else 0
    }

    return metrics


def metric_scores(prediction, ground_truth):
    """ Useful cores for various metrics.

    :param prediction: Detected bead seeds.
        :type prediction:
    :param ground_truth: Ground truth image with intensity-coded beads.
        :type ground_truth:
    :return: Dictionary with the metric scores
                'N_gt': number of ground truth beads in the test image,
                'N_pred': number of predicted beads in the test image,
                'N_split': number of split beads in the test image (multiple predicted beads lies in a gt bead),
                'N_miss': number of missing beads in the test image (no predicted bead lies in a gt bead),
                'N_add': number of added beads in the test image (predicted bead lies in gt background),
                'tp': true positives in the test image (N_pred - N_split - N_add),
                'fp': false positives in the test image (N_split + N_add),
                'fn': false negatives in the test image (N_miss),
                'Q_P': Precision of the test image i,
                'Q_R': Recall of the test image i,
                'Q_F': F-score of the test image i.
    """

    # Assign each predicted bead seed a unique id
    prediction = measure.label(prediction, connectivity=1, background=0)
    
    # Get the ground truth bead ids and the prediction bead seeds ids
    bead_ids_gt, bead_ids_pred = get_bead_ids(ground_truth), get_bead_ids(prediction)

    # Preallocate lists for used beads and for split beads
    used_ids_pred, used_ids_gt, split_ids_pred, split_ids_gt = [], [], [], []

    # Estimate the predicted bead seeds which lie in a ground truth bead (intensity-coded to get the gt bead ids)
    seeds_in_gt = ground_truth * (prediction > 0)
    seeds_in_gt_hist = np.histogram(seeds_in_gt, bins=range(1, bead_ids_gt[-1] + 2), range=(1, bead_ids_gt[-1] + 1))

    # Get prediction bead seed ids which match a gt bead (regardless of splits, ...)
    used_ids_pred.append(get_bead_ids(prediction * (seeds_in_gt > 0)))

    # Get gt bead ids which have (at least one) predicted bead seed inside
    used_ids_gt.append(get_bead_ids(seeds_in_gt))

    # Find split beads (ids of the multiple predicted seeds and of the corresponding gt bead)
    for i, num_nuclei in enumerate(seeds_in_gt_hist[0]):
        if num_nuclei > 1:
            split_ids_gt.append(seeds_in_gt_hist[1][i])
            split_ids_pred.append(get_bead_ids(prediction * (seeds_in_gt == seeds_in_gt_hist[1][i])))

    # Count split beads and check for multiple splits within one ground truth bead (the splitting of a gt bead into
    # three seeds is counted as two splits, ...)
    num_split = 0
    for i in range(len(split_ids_pred)):
        num_split += len(split_ids_pred[i]) - 1

    # Find missing beads (gt bead not marked as used or split)
    num_missing = 0
    ids_gt = list(chain.from_iterable(used_ids_gt)) + split_ids_gt
    for bead_id in bead_ids_gt:
        if bead_id not in ids_gt:
            num_missing += 1

    # Find added beads (predicted seed not marked as used or split)
    num_added = 0
    ids_pred = list(chain.from_iterable(used_ids_pred)) + list(chain.from_iterable(split_ids_pred))
    for bead in bead_ids_pred:
        if bead not in ids_pred:
            num_added += 1

    # Calculate true positives, false positives and false negatives
    tp = len(bead_ids_pred) - num_split - num_added
    fp = num_split + num_added
    fn = num_missing

    # Precision, recall and f_score of the image
    precision = tp / (tp + fp) if (tp + fp) > 0 else 0
    recall = tp / (tp + fn) if (tp + fn) > 0 else 0
    f_score = 2 * precision * recall / (precision + recall) if (recall + precision) > 0 else 0

    scores = {'N_gt': len(bead_ids_gt),
              'N_pred': len(bead_ids_pred),
              'N_split': num_split,
              'N_miss': num_missing,
              'N_add': num_added,
              'tp': tp,
              'fp': fp,
              'fn': fn,
              'Q_P': precision,
              'Q_R': recall,
              'Q_F': f_score}

    return scores

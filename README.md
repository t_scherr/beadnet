# BeadNet: Deep Learning-Based Bead Detection and Counting in Low-Resolution Microscopy Images

**BeadNet v2 with OMERO data management is now [available](https://github.com/TimScherr/BeadNet-v2)**!

BeadNet is a U-Net-based software for the detection and counting of small poorly-resolved objects in images, e.g., ligand-coupled beads. An annotated low-resolution bead data set and 10 pre-trained networks can be found in the [downloads](https://bitbucket.org/t_scherr/beadnet/downloads/) section. For Windows users, we suggest to use the frozen binary executable.

![Prediction overlay](documentation/inference_test_1_upsampled_seed_overlay.png) ![Prediction overlay](documentation/inference_test_2_upsampled_seed_overlay.png)

## Prerequisites

### Frozen Binary Executable

* Operating system: Windows 10
* For GPU use: a CUDA capable GPU
* Minimum / recommended RAM/VRAM: 8 GiB / 16 GiB
* Supported data formats: .tif, .tiff, .png, .jpg
* [7zip](https://www.7-zip.de/)

### Python Code

* Operating system: Windows (tested on Windows 10) or Linux (tested on Ubuntu 18.04)
* [Anaconda Distribution](https://www.anaconda.com/distribution/#download-section)
* For GPU use: a CUDA capable GPU
* Minimum / recommended RAM/VRAM: 8 GiB / 16 GiB
* Supported data formats: .tif, .tiff, .png, .jpg

## Installation

### Frozen Binary Executable

Download "BeadNet_for_Windows.7z.001" and "BeadNet_for_Windows.7z.002" in the [downloads](https://bitbucket.org/t_scherr/beadnet/downloads/) section. Unzip "BeadNet_for_Windows.7z.001" with 7zip.

Run beadnet.exe. The graphical user interface opens (may take a while depending on your system): 

![GUI](documentation/gui.png)

### Python Code

Clone the BeadNet repository:

```
git clone https://bitbucket.org/t_scherr/beadnet.git
```

Download the BeadNet data set, the inference test images, and the trained models in the [downloads](https://bitbucket.org/t_scherr/beadnet/downloads/) section. Download the [image labeling tool executable](https://bitbucket.org/abartschat/imagelabelingtool/downloads/) for your operating system (tested on Windows 10, Ubuntu 18.04 and Manjaro Linux 18.1.5).

Unzip all files in the BeadNet repository.

Open the Anaconda Prompt (Windows) or Terminal (Linux), go to the BeadNet repository and create a new virtual environment:

```
cd path_to_your_cloned_beadnet_repository
conda env create -f requirements.yml
```

Activate the beadnet_ve:

```
conda activate beadnet_ve
```

Run the file beadnet.py:

```
python beadnet.py
```

The graphical user interface opens:

![GUI](documentation/gui.png)

## Getting Started with BeadNet

In this section, we describe how to use BeadNet. BeadNet comes with an annotated data set for training and evaluation and three microscopy images for inference. So, the software can also be tested without own data.

### Frozen Binary Executable

Run the executable and use the graphical user interface.

### Python Code

If the BeadNet mode in the file settings.json is set to "gui", the graphical user interface will be used. Users who don't want to use the graphical user interface, can set the mode to "train", "eval", "inference" and "inference_ensemble" and specify the paths in the settings file. Here, we assume that the graphical user interface is used.

### BeadNet Data Set

The BeadNet data set consists of 100 grayscale images (128x128 px) showing 2587 annotated beads. The data set is divided into a training, a validation and a test subset. Each subset consists of original low-resolution images, upsampled images, annotated seed images, dilated annotated seed images, and intensity-coded enlarged seed images for metric score calculations. For more information about the data set, take a look into the supplementary information of our [publication](#markdown-header-publication).

|![Image](documentation/005_img.png)|![Upsampled Image](documentation/005_img_upsampled.png)|![Seeds](documentation/005_seeds.png)|![Dilated seeds](documentation/005_seeds_dilated.png)|![Metric seeds](documentation/005_seeds_metric.png)
|-----------------------------------|-------------------------------------------------------|-------------------------------------|-----------------------------------------------------|---------------------------------------------------
|Image|Upsampled image|Seeds|Dilated seeds|Metric seeds

|![Image](documentation/013_img.png)|![Upsampled Image](documentation/013_img_upsampled.png)|![Seeds](documentation/013_seeds.png)|![Dilated seeds](documentation/013_seeds_dilated.png)|![Metric seeds](documentation/013_seeds_metric.png)
|-----------------------------------|-------------------------------------------------------|-------------------------------------|-----------------------------------------------------|---------------------------------------------------
|Image|Upsampled image|Seeds|Dilated seeds|Metric seeds

### Paths

* *Data dir*: directory of the data to analyze
* *Model dir*: directory containing the trained models / to save new models into
* *Training dir*: directory of an annotated data set (see [Create New Training Data](#markdown-header-create-new-training-data) and [Data Formats and Normalization](#markdown-header-data-formats-and-normalization) if you want to use your own data)

### Settings
* *Color channel*: color channel being processed of the data to analyze (grayscale converts RGB images into grayscale images) 
* *Diameter range*: bead diameter range in px (used to select the upsampling factor for the data in *data dir*). If not known: measure it using [Fiji](https://imagej.net/Fiji). Currently, the following cases are distinguished: 1px - 4px (fourfold bilinear upsampling), 5px - 8px (twofold bilinear upsampling), > 8px (no upsampling) 
* *Device*: device to use for training/evaluation/inference (*gpu* can only be checked if a CUDA capable GPU is detected) 
* *Ensemble mode*: use multiple models for inference

### Evaluate Trained Models

During evaluation, all trained models in *model dir* are evaluated on the test subset of the annotated data set (*training dir*). The metrics of each model on this data set with and without border correction (see our [publication](#markdown-header-publication) for more details) are saved in json files in *model dir*. In addition, the predicted seeds and a seeds-ground-truth-overlay are saved for each model. To be sure that the models apply well on your own data, it may be helpful to annotate some data  (see [Create New Training Data](#markdown-header-create-new-training-data)).

|![GUI](documentation/gui_evaluation.png)|![Results](documentation/metrics.png)
|----------------------------------------|-------------------------------------
|Graphical user interface during evaluation|Metrics json file (border corrected)

|![Image](documentation/015_img_zoom.png)|![Upsampled Image](documentation/015_img_upsampled.png)|![Seeds metrics](documentation/015_seeds_metric.png)|![Prediction overlay](documentation/015_img_upsampled_bead_seeds_bc_overlay.png)
|----------------------------------------|-------------------------------------------------------|----------------------------------------------------|--------------------------------------------------------------------------------
|Image|Upsampled image|Metric seeds|Prediction overlay

|![Image](documentation/019_img_zoom.png)|![Upsampled Image](documentation/019_img_upsampled.png)|![Seeds metrics](documentation/019_seeds_metric.png)|![Prediction overlay](documentation/019_img_upsampled_bead_seeds_bc_overlay.png)
|----------------------------------------|-------------------------------------------------------|----------------------------------------------------|--------------------------------------------------------------------------------
|Image|Upsampled image|Metric seeds|Prediction overlay

### Inference

During inference, the model with the best micro F-score (see the supplementary information of our [publication](#markdown-header-publication)) on the test subset is selected and predictions of all images in *data dir* are made. The following files are saved: the upsampled images (depending on the selected *diameter range*), the predicted seeds, an predicted seeds - upsampled image overlay, and a json file containing the number of predicted beads for each processed image. If not enough RAM/VRAM is available and for large images, a sliding window approach is applied automatically. For the provided inference test images, the diameter range needs to be set to 1px - 4px.

|![GUI](documentation/gui_inference.png)|![Results](documentation/inference_results.PNG)
|---------------------------------------|-----------------------------------------------
|Graphical user interface during inference|Results json file (number of beads)

|![Low-resolution image](documentation/inference_test_1_zoom.png)|![Upsampled image](documentation/inference_test_1_upsampled.png) |![Prediction overlay](documentation/inference_test_1_upsampled_seed_overlay.png)
|----------------------------------------------------------------|-----------------------------------------------------------------|--------------------------------------------------------------------------------
|Low-resolution image|Upsampled image|Prediction overlay

|![Low-resolution image](documentation/inference_test_rgb_zoom.png)|![Upsampled image](documentation/inference_test_rgb_upsampled.png) |![Prediction overlay](documentation/inference_test_rgb_upsampled_seed_overlay.png)
|------------------------------------------------------------------|-------------------------------------------------------------------|----------------------------------------------------------------------------------
|Low-resolution image|Upsampled image (grayscale conversion mode)|Prediction overlay

### Inference (Ensemble Mode)

In the ensemble mode, not only the best but the three best models are used for inference. The results of each ensemble model are saved.

### Train New Models

New models are trained on the data in *training dir*. For each iteration, a model is trained using the seeds and a model using the dilated seeds. The number of epochs can be reduced to decrease the training time for tests. The batch size can be set to 1 if not enough RAM/VRAM is available. Don't forget to *evaluate* the new models.

|![Training settings](documentation/training_settings.png)|![GUI](documentation/gui_training.png)
|---------------------------------------------------------|--------------------------------------
|Training settings|Graphical user interface during training

### Create New Training Data

If BeadNet does not generalize well to your data, you may need to create a new data set. For that, random crops of the data in the specified data path can be generated (*Create new crops*). The crops are upsampled depending on the specified *diameter range*. To avoid that the final crops show only/almost background, the user needs to *accept* or *reject* the proposed crops. Rejected crops will not be shown again until BeadNet is restarted.

|![Create train data settings](documentation/gui_create_train_data.png)|![Create train data menu](documentation/gui_create_train_data_dialog.png)|![Exemplary crop](documentation/gui_create_train_data_crop_1.png)|![Exemplary crop](documentation/gui_create_train_data_crop_2.png)
|----------------------------------------------------------------------|-------------------------------------------------------------------------|-----------------------------------------------------------------|-----------------------------------------------------------------
|Settings|Menu|Exemplary crop (accept)|Exemplary crop (reject)

The accepted crops are saved in *training dir* and can be annotated using the image labeling tool (*Annotate* button):

* change color: press number of color,
* annotate/paint: left-click / left-click + move the mouse,
* paint over: ctrl + left-click
* change pen size: use mouse wheel or the slider,
* zoom: press ctrl + mouse wheel,
* move image: right-click and move the mouse,
* undo: press ctrl + z,
* save: press ctrl + s or go to previous/next image,
* go to previous/next image: press arrow keys,
* Recommendations: 
    * use pen size 2 or 3 (or bigger, but seeds should not touch at edges)
    * select just one color (not background color),
    * remove wrong annotations by painting over with the background color
    * Don't forget to save after annotating the last image.

Annotated images need to be converted into a data set (*Convert to data set*). The seed, dilated seed and metric seed images are created automatically. After the conversion to a data set, the data can be used to train new models. If *training dir* already contains a data set, the new annotations are simply added to it. So, the provided BeadNet data set can be used as basic data set.

## Data Formats and Normalization

### BeadNet Data Set
The BeadNet data set is divided into a training, a validation and a test subset. To be able to use the PyTorch BeadNet data set to load your own data for the training process (in case you don't want to use our create new training data function or you already have annotated data), your data need

* to be single grayscale tiff-files with shape (*height*, *width*, 1) and *height* & *width* to be a power of 2,
* to be divided into a "train", a "val", and a "test" directory,
* to contain an image called "*_img_upsampled.tif",
* to contain an annotated seed image called "*_seeds.tif",
* to contain a dilated annotated seed image called "*_seeds_dilated.tif",
* to contain an enlarged intensity-coded annotated seed image for metric calculation called "*_seeds_metric.tif",
* to be uint16 images best normalized to the full uint16 range [0, 65535] (not necessary for the seeds).

If you want to use the *Convert to data set* functionality of BeadNet, your data need

* to be single grayscale tiff-files with shape (height, width, 1) or (*height*, *width*) *height* & *width* to be a power of 2,
* to contain an image called "*_img_upsampled.tif",
* to contain an annotated label image called "*_img_upsampled_label.tif",
* to be uint16 images best normalized to the full uint16 range [0, 65535] (not necessary for the label image).

It is avoided to add already used images multiple times to the created data set. So, the *Convert to data set* functionality can be easily used multiple times after adding new annotated images to the directory.

### Inference
BeadNet supports .tif, .tiff, .png and .jpg images for inference. All images are min-max-normalized and converted to uint16 images to enable a consistent workflow. Tiff-stacks are not supported. Thus, we kindly ask the user to split the stacks into single files , e.g., using [Fiji](https://imagej.net/Fiji). If the input images are RGB-images, the channel being processed can be selected ("grayscale" converts an RGB image to a grayscale image).

### Help
Press F1 to open the help:

![Help](documentation/help.png)

## Publication
T. Scherr, K. Streule, A. Bartschat, M. B�hland, J. Stegmaier, M. Reischl, V. Orian-Rousseau and R. Mikut (2020), BeadNet: Deep Learning-Based Bead Detection and Counting in Low-Resolution Microscopy Images, *Bioinformatics*, doi:[10.1093/bioinformatics/btaa594](https://doi.org/10.1093/bioinformatics/btaa594).

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

## Appendix

### Exemplary Application: Cell Nuclei Counting
BeadNet can not only be used for the intial intended use case of low-resolution objects or beads in microscopy images, but also for counting other objects which are well-resolved, e.g. cell nuclei. To show this, we use the image set [BBBC004v1](https://data.broadinstitute.org/bbbc/BBBC004/) [Ruusuvuori et al., in Proc. of the 16th European Signal Processing Conference (EUSIPCO-2008), 2008] from the Broad Bioimage Benchmark Collection [[Ljosa et al., Nature Methods, 2012](http://dx.doi.org/10.1038/nmeth.2083)]. 

The approximate cell nucleus diameter in this data set is about 20 px which means that no upsampling is needed and that the objects are even larger than the upsampled beads in our BeadNet data set. The application of our provided models (trained on the low-resolution BeadNet data set) results in an over- or underestimation of cell nuclei:

|![Training settings](documentation/10GRAY_crop_beadnet_model_p_dilation_002_seeds_overlay.png)
|---------------------------------------------------------
|BeadNet detection of a 300x500 px crop from the BBBC004_v1_000 image "10GRAY" 

A simple approach to improve the cell nucleus detection is to downsample the image (here: to 150x250 px resulting in 10 px cell nucleus diameter):

|![Training settings](documentation/10GRAY_crop_downsampled_beadnet_model_p_dilation_002_seeds_overlay.png)
|---------------------------------------------------------
|BeadNet detection of the downsampled crop

Now, the cell nuclei can be detected well without creating new training data and training new networks. However, for the creation and annotation of a new training data set consisting of 25 crops (128x128 px, taken from the BBBC004_v1_000 images "1GRAY" and "20GRAY") showing 140 cell nuclei in total, we needed less than 10 minutes with BeadNet. The results of the newly trained models show also a good cell nucleus detection: 

|![Training settings](documentation/10GRAY_crop_beadnet_model_003_seeds_overlay.png)
|---------------------------------------------------------
|BeadNet detection with a newly trained model
